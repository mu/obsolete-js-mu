lazy val microvmRefimpl2 = file("js-mu-priv-mr2")

lazy val root = project.in(file(".")).dependsOn(microvmRefimpl2).settings(
  organization := "org.microvm",
  name := "js-mu",
  description := "A subset of JavaScript on Mu micro VM",
  scalaVersion := "2.11.6",
  libraryDependencies := Seq(
    "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
    "ch.qos.logback" % "logback-classic" % "1.1.2",
    "org.scalatest" %% "scalatest" % "2.2.0"
  ),
  EclipseKeys.createSrc := EclipseCreateSrc.Default + EclipseCreateSrc.Resource + EclipseCreateSrc.Managed
)
