package org.microvm.jsmu.muir

import scala.collection.mutable.ArrayBuffer

class MuIRFunctionBuilder(val muName: String, val sig: String, val muParams: Seq[String]) {
  val func = new MuFunc(muName, muParams)

  val entry = new MuBB(muName + ".entry")
  func.bbs += entry
  var curBB = entry

  val bbNamer = new MuNamer(muName + ".bb")
  val instNamer = new MuNamer(muName + ".inst")

  def add(inst: MuInst) {
    curBB.insts += inst
  }

  def emit(hint: String, instBody: String): String = {
    val instName = instNamer.next(hint)
    val inst = new MuInst(instName, instBody)
    add(inst)

    instName
  }

  def use(bb: MuBB) {
    curBB = bb
  }

  def newBB(hint: String): MuBB = {
    val bbMuName = bbNamer.next(hint)
    val bb = new MuBB(bbMuName)
    func.bbs += bb
    bb
  }
}

object MuIRFunctionBuilder {

}

trait SBBuildable {
  def toMuIR(sb: StringBuilder)

  def toMuIR(): String = {
    val sb = new StringBuilder()
    toMuIR(sb)
    sb.toString()
  }
}

class MuBundle extends SBBuildable {
  val funcs = ArrayBuffer[MuFunc]()

  def toMuIR(sb: StringBuilder) {
    for (func <- funcs) {
      func.toMuIR(sb)
    }
  }
}

class MuFunc(val muName: String, val muParams: Seq[String]) extends SBBuildable {
  val bbs = ArrayBuffer[MuBB]()

  def toMuIR(sb: StringBuilder) {
    sb ++= s".funcdef ${muName} VERSION ${muName}.v1 <@js.userfunc.sig> (${muParams.mkString(" ")}) {\n"

    for (bb <- bbs) {
      bb.toMuIR(sb)
    }

    sb ++= "}\n"
  }
}

class MuBB(val muName: String) extends SBBuildable {
  val insts = ArrayBuffer[MuInst]()

  def toMuIR(sb: StringBuilder) {
    sb ++= s"  ${muName}:\n"

    for (inst <- insts) {
      inst.toMuIR(sb)
    }
  }
}

class MuInst(val muName: String, val body: String) extends SBBuildable {
  def toMuIR(sb: StringBuilder) {
    sb ++= s"    ${muName} = ${body}\n"
  }
}

class MuNamer(val prefix: String) {
  import MuNamer._
  var num = 0

  def next(): String = {
    val myNum = num
    num += 1
    return prefix + num.toString
  }

  def next(nameHint: String): String = {
    val myNum = num
    num += 1
    return prefix + num.toString + "_" + sanitize(nameHint)
  }
}
object MuNamer {
  val legalChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_."

  def sanitize(name: String): String = {
    name.filter(ch => legalChars.contains(ch))
  }
}

object ImplicitConversions {
  implicit class MagicString(s: String) {
    def %(local: String): String = {
      s + "." + local
    }
  }
}
