package org.microvm.jsmu.client

import scala.collection.mutable.Map
import uvm.refimpl.MicroVM
import org.microvm.jsmu.muir._
import org.microvm.jsmu.client.MicroVMHelpers._
import uvm.ssavariables.MemoryOrder
import org.slf4j.LoggerFactory
import com.typesafe.scalalogging.Logger

object GlobalPool {
  val logger: Logger = Logger(LoggerFactory.getLogger(getClass.getName))
}

class GlobalPool(jsClient: JSClient) {
  import GlobalPool._

  val ca = jsClient.ca

  val globalNamer = new MuNamer("@global")

  val globalPool = Map[String, String]()
  val profCounterPool = Map[String, String]()

  private implicit def idOf(name: String): Int = jsClient.microVM.idOf(name)
  private implicit def nameOf(id: Int): String = jsClient.microVM.nameOf(id)

  def getGlobalCell(varName: String): String = {
    globalPool.getOrElseUpdate(varName, {
      val muName = globalNamer.next(varName)

      val miniBundle = s""".global ${muName} <@js.value>"""

      logger.debug("Loading:\n" + miniBundle)
      ca.loadBundle(miniBundle)

      logger.debug("Defined global '[%d:%s]' for global variable %s".format(idOf(muName), muName, varName))

      val hNull = ca.putConstant("@NULL")
      val hUndefTag = ca.putConstant("@js.undef.tag")
      val hUndefVal = ca.tr64FromRef(hNull, hUndefTag)
      val hGlobal = ca.putGlobal(muName)
      ca.store(MemoryOrder.NOT_ATOMIC, hGlobal, hUndefVal)

      ca.deleteHandles(hNull, hUndefTag, hUndefVal, hGlobal)

      muName
    })
  }

  def getProfCounterFor(funcMuName: String): String = {
    profCounterPool.getOrElseUpdate(funcMuName, {
      val muName = funcMuName + ".counter"

      val miniBundle = s""".global ${muName} <@i64>"""

      logger.debug("Loading:\n" + miniBundle)
      ca.loadBundle(miniBundle)

      logger.debug("Defined global '[%d:%s]' for the profiling counter of %s".format(idOf(muName), muName, funcMuName))
      
      // already initialized as 0

      muName
    })

  }
}
