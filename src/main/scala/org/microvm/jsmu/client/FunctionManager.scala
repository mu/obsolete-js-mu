package org.microvm.jsmu.client

import scala.collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap

import org.microvm.jsmu.compiler.baseline.BaselineFunction
import org.microvm.jsmu.compiler.opt.OptimizedFunction

import jdk.nashorn.internal.ir.FunctionNode
import jdk.nashorn.internal.runtime.Source

class FunctionManager(val jsClient: JSClient) {
  /** Maps funcMuName to PrimitiveFunction. */
  val primitiveFunctions = new HashMap[String, PrimitiveFunction]()
  val jsFunctions = new HashMap[FunctionNode, JSFunction]()
}

/**
 * Something callable from JS code.
 *
 * @param nameHint A human-readable name for debugging. Not for identifying a JSCallable.
 */
abstract class JSCallable(val nameHint: String)

/**
 * A primitive function, defined directly in MuIR with the @js.userfunc.sig signature.
 *
 * @param funcMuName: The MuIR name of the function.
 * @param constMuName: THe MuIR name of the global cell that holds a reference to a closure object.
 */
class PrimitiveFunction(nameHint: String, val funcMuName: String, val constMuName: String) extends JSCallable(nameHint)

/**
 * A JS function compiled from a JS script.
 */
class JSFunction(nameHint: String, val source: Source, val funcNode: FunctionNode) extends JSCallable(nameHint) {
  /** A list of JS parameters. */
  val params: Seq[String] = funcNode.getParameters.map(_.getName).toSeq
  
  /** A list of variables declared by "var" in the function. */
  val localVars = new ArrayBuffer[String]()
  
  /**
   * Get all local variables, including params and local vars.
   * Stack introspection depends on this order.
   */
  def getAllLocalVars() = params ++ localVars

  // The following fields have Option[T] types. If they are None, they are not generated yet.

  var blFunc: Option[BaselineFunction] = None
  var optFunc: Option[OptimizedFunction] = None
}
