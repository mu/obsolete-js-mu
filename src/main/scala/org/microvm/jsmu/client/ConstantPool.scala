package org.microvm.jsmu.client

import scala.collection.mutable.Map
import uvm.refimpl.MicroVM
import org.microvm.jsmu.muir._
import org.microvm.jsmu.client.MicroVMHelpers._
import uvm.ssavariables.MemoryOrder
import org.slf4j.LoggerFactory
import com.typesafe.scalalogging.Logger

object ConstantPool {
  val logger: Logger = Logger(LoggerFactory.getLogger(getClass.getName))
}

class ConstantPool(jsClient: JSClient) {
  import ConstantPool._

  val ca = jsClient.ca

  val constNamer = new MuNamer("@const")

  val stringPool = Map[String, String]()
  val longPool = Map[Long, String]()
  val doublePool = Map[Double, String]()
  val closurePool = Map[String, String]()

  private implicit def idOf(name: String): Int = jsClient.microVM.idOf(name)
  private implicit def nameOf(id: Int): String = jsClient.microVM.nameOf(id)

  def getStringConstant(string: String): String = {
    stringPool.getOrElseUpdate(string, {
      val muName = constNamer.next()

      val miniBundle = s""".global ${muName} <@js.value>"""

      logger.debug("Loading:\n" + miniBundle)
      ca.loadBundle(miniBundle)

      logger.debug("Defined global '[%d:%s]' for string const \"%s\"".format(idOf(muName), muName, string))

      val strLen = string.length()

      val hStrLen = ca.putInt("@i64", strLen)
      val hStrObj = ca.newHybrid("@js.string", hStrLen)
      val hStrObjIr = ca.getIRef(hStrObj)
      val hStrLenIr = ca.getFixedPartIRef(hStrObjIr)
      ca.store(MemoryOrder.NOT_ATOMIC, hStrLenIr, hStrLen)

      val hStrVarIr = ca.getVarPartIRef(hStrObjIr)

      for (i <- 0 until strLen) {
        val hIndex = ca.putInt("@i64", i)
        val hCharIr = ca.shiftIRef(hStrVarIr, hIndex)
        val ch = string.charAt(i)
        val hCh = ca.putInt("@i8", ch)
        ca.store(MemoryOrder.NOT_ATOMIC, hCharIr, hCh)

        ca.deleteHandles(hIndex, hCharIr, hCh)
      }

      val hGlobal = ca.putGlobal(muName)
      val hStrTag = ca.putConstant("@js.string.tag")
      val hTr = ca.tr64FromRef(hStrObj, hStrTag)
      ca.store(MemoryOrder.NOT_ATOMIC, hGlobal, hTr)

      ca.deleteHandles(hStrLen, hStrObj, hStrObjIr, hStrLenIr, hStrVarIr, hGlobal, hStrTag, hTr)

      muName
    })
  }

  def getLongConstant(v: Long): String = {
    longPool.getOrElseUpdate(v, {
      val muName = constNamer.next()

      val miniBundle = s""".const ${muName} <@i64> = ${v.toString}"""

      logger.debug("Loading:\n" + miniBundle)
      ca.loadBundle(miniBundle)

      muName
    })
  }

  def getDoubleConstant(v: Double): String = {
    doublePool.getOrElseUpdate(v, {
      val muName = constNamer.next()

      val vRaw = java.lang.Double.doubleToRawLongBits(v)
      val miniBundle = s""".const ${muName} <@double> = bitsd(0x${vRaw.toHexString}) // ${v.toString()}"""

      logger.debug("Loading:\n" + miniBundle)
      ca.loadBundle(miniBundle)

      muName
    })
  }

  def getBaselineClosureConstant(jsFunc: JSFunction): String = {
    val funcMuName = jsFunc.blFunc.get.muFunc.muName
    getClosureConstant(funcMuName)
  }

  def getOptimizedClosureConstant(jsFunc: JSFunction): String = {
    val funcMuName = jsFunc.optFunc.get.muFunc.muName
    getClosureConstant(funcMuName)
  }

  def getClosureConstant(funcMuName: String): String = {
    closurePool.getOrElseUpdate(funcMuName, {
      val muName = constNamer.next()

      val miniBundle = s""".global ${muName} <@js.value>"""

      logger.debug("Loading:\n" + miniBundle)
      ca.loadBundle(miniBundle)

      val hClosure = ca.newFixed("@js.closure")
      val hClosureIr = ca.getIRef(hClosure)
      val hClosureFuncIr = ca.getFieldIRef(hClosureIr, 0)
      val hFunc = ca.putFunction(funcMuName)
      ca.store(MemoryOrder.NOT_ATOMIC, hClosureFuncIr, hFunc)

      val hGlobal = ca.putGlobal(muName)
      val hClosureTag = ca.putConstant("@js.closure.tag")
      val hClosureTr = ca.tr64FromRef(hClosure, hClosureTag)
      ca.store(MemoryOrder.NOT_ATOMIC, hGlobal, hClosureTr)
      ca.deleteHandles(hClosure, hClosureIr, hClosureFuncIr, hFunc, hGlobal, hClosureTag, hClosureTr)

      muName
    })
  }
}
