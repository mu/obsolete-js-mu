package org.microvm.jsmu.client

object JSTags {
  val TAG_UNDEF = 0
  val TAG_NULL = 1
  val TAG_FALSE = 2
  val TAG_TRUE = 3
  //  val TAG_DOUBLE = 4  // Double is represented as a tr64 double, not tr64 ref to double
  val TAG_STRING = 5
  val TAG_CLOSURE = 6
}
