package org.microvm.jsmu.client

import java.io.FileReader

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap

import org.microvm.jsmu.compiler.JSTypes
import org.microvm.jsmu.compiler.JSTypes.JSTypes
import org.microvm.jsmu.compiler.SourceHelper
import org.microvm.jsmu.compiler.baseline.BackEdgeInfo
import org.microvm.jsmu.compiler.baseline.BaselineCompiler
import org.microvm.jsmu.compiler.opt.OSRInfo
import org.microvm.jsmu.compiler.opt.OptimizingCompiler
import org.slf4j.LoggerFactory

import com.typesafe.scalalogging.Logger

import MicroVMHelpers._
import jdk.nashorn.internal.ir.Node
import uvm.refimpl._
import uvm.ssavariables.MemoryOrder._

object JSClient {
  val logger: Logger = Logger(LoggerFactory.getLogger(getClass.getName))
}

class JSClient(val microVM: MicroVM) {
  import JSClient._

  val ca = microVM.newClientAgent() // For initialiser/compiler/loader to use.

  val constantPool = new ConstantPool(this)
  val globalPool = new GlobalPool(this)
  val funcManager = new FunctionManager(this)

  private implicit def idOf(name: String): Int = microVM.idOf(name)
  private implicit def nameOf(id: Int): String = microVM.nameOf(id)

  {
    // Load base bundle. Contain primitive Mu definitions, including basic type definitions.
    val preloadReader = new FileReader("uir/jsbase.uir")
    try {
      ca.loadBundle(preloadReader)
    } finally {
      preloadReader.close()
    }
  }

  def registerGlobalFunction(funcJSName: String, funcMuName: String): Unit = {
    // It creates two global cells, one for a "constant closure value" and the other for the
    // "global variable" that holds this value. It is indeed a waste of space because no expressions
    // in any JS code will ever hold the "constant closure value". I just made it consistent with
    // "other anonymous functions (var foo = function () {...}) defined in the code".
    val constMuName = constantPool.getClosureConstant(funcMuName)
    val globalMuName = globalPool.getGlobalCell(funcJSName)

    val hGlobal = ca.putGlobal(globalMuName)
    val hClosureTr = ca.putGlobal(constMuName)
    val hClosureVal = ca.load(NOT_ATOMIC, hClosureTr)
    ca.store(NOT_ATOMIC, hGlobal, hClosureVal)
    ca.deleteHandles(hGlobal, hClosureTr, hClosureVal)

    val pf = new PrimitiveFunction(funcJSName, funcMuName, constMuName)
    funcManager.primitiveFunctions(funcMuName) = pf
  }

  {
    registerGlobalFunction("print", "@js.print")
  }

  /**
   * Map from Mu names to their JSRuntimeError entries.
   * The Mu name is the CALLER of the @js.error function.
   * The trap handler of the trap in @js.error will lookup this table and print error messages.
   */
  val jsRuntimeInstInfoMap = new HashMap[String, JSRuntimeInstInfo[Node]]()

  def registerRuntimeInstInfo(muName: String, runtimeError: JSRuntimeInstInfo[Node]): Unit = {
    if (jsRuntimeInstInfoMap.contains(muName)) {
      throw new JSClientException("Re-added runtime error entry for '%s'".format(muName))
    }
    jsRuntimeInstInfoMap(muName) = runtimeError
  }

  def getRuntimeInstInfo(muName: String): JSRuntimeInstInfo[Node] = {
    jsRuntimeInstInfoMap.getOrElse(muName, {
      throw new JSRuntimeException("JSRuntimeInstInfo for '%s' does not exist. Bug in BaselineCompiler or JSClient".format(muName))
    })
  }

  /**
   * Map from Mu names of the trap instruction in optimization blocks in baseline functions to their BackEdgeInfo
   * instances.
   */
  val backEdgeInfoMap = new HashMap[String, BackEdgeInfo]()

  val baselineCompiler = new BaselineCompiler(this)
  val optimizingCompiler = new OptimizingCompiler(this)

  object trapHandler extends TrapHandler() {
    def handleTrap(ca: ClientAgent, thread: Handle, stack: Handle, watchPointID: Int): TrapHandlerResult = {
      val curInst = ca.currentInstruction(stack, 0)
      val instName = nameOf(curInst)
      instName match {
        case "@js.print.v1.trap" => { // The "print" function
          val Seq(hParams) = ca.dumpKeepalives(stack, 0)

          handlePrint(hParams)

          ca.deleteHandle(hParams)

          TrapRebindPassVoid(stack)
        }
        case "@js.error.v1.trap" => { // The "error" function
          val s1InstName = nameOf(ca.currentInstruction(stack, 1))

          s1InstName match {
            case "@js.add.gen.v1.error_call"
              | "@js.sub.gen.v1.error_call"
              | "@js.mul.gen.v1.error_call"
              | "@js.div.gen.v1.error_call"
              | "@js.eqs.gen.v1.error_call"
              | "@js.lt.gen.v1.error_call" => handleBinOpTypeError(ca, stack)
            case "@js.not.gen.v1.error_call"
              | "@js.neg.gen.v1.error_call" => handleUnOpTypeError(ca, stack)
            case "@js.truth.v1.error_call" => {
              val s2InstName = nameOf(ca.currentInstruction(stack, 2))
              val s2ri = getRuntimeInstInfo(s2InstName)
              s2ri.kind match {
                case TruthTest(expr) => {
                  val Seq(cond) = ca.dumpKeepalives(stack, 1)
                  println("Runtime error: Value not suitable for truth test.")
                  printValueWithType(ca, cond, "condition")

                  printMarker(s2ri)
                }
              }
            }
            case _ => {
              val s1ri = getRuntimeInstInfo(s1InstName)
              s1ri.kind match {
                case CalleeNotClosure(cn) => {
                  val Seq(callee) = ca.dumpKeepalives(stack, 1)
                  println("Runtime error: Callee is not a closure.")
                  printValueWithType(ca, callee, "callee")

                  printMarker(s1ri)
                }
                case i: InternalOp => handleInternalOp(i)
              }
            }
          }

          println("Low-level stack trace:")
          lowLevelStackTrace(ca, stack, 0)
          TrapExit()
        }

        case _ => {
          if (backEdgeInfoMap.contains(instName)) {
            val bei = backEdgeInfoMap(instName)
            handleOptTrap(bei, ca, stack, thread, watchPointID)
          } else {
            throw new JSRuntimeException("Unhandled trap " + instName)
          }
        }
      }
    }

    private def handleBinOpTypeError(ca: ClientAgent, stack: Handle) {
      val Seq(lhs, rhs) = ca.dumpKeepalives(stack, 1)
      val s2InstName = nameOf(ca.currentInstruction(stack, 2))
      val s2ri = getRuntimeInstInfo(s2InstName)
      s2ri.kind match {
        case BinaryOp(_) => {
          println("Runtime error: inappropriate or unimplemented types for this kind of binary operation.")
          printValueWithType(ca, lhs, "lhs")
          printValueWithType(ca, rhs, "rhs")
        }
        case UnaryIncDec(_) => {
          println("Runtime error: inappropriate or unimplemented types for unary inc/dec.")
          printValueWithType(ca, lhs, "opnd")
        }
      }
      printMarker(s2ri)
    }

    private def handleUnOpTypeError(ca: ClientAgent, stack: Handle) {
      val Seq(opnd) = ca.dumpKeepalives(stack, 1)
      println("Runtime error: inappropriate or unimplemented type for this kind of unary operation.")
      printValueWithType(ca, opnd, "opnd")

      val s2InstName = nameOf(ca.currentInstruction(stack, 2))
      val s2ri = getRuntimeInstInfo(s2InstName)
      s2ri.kind match {
        case i: InternalOp => handleInternalOp(i)
        case _             =>
      }
      printMarker(s2ri)
    }

    private def handleInternalOp(i: InternalOp) {
      println("This is an internal operation: ''. It is a bug in the JS client.".format(i.desc))

    }

    private def handlePrint(hParams: Handle) {
      val hIr = ca.getIRef(hParams)
      iterHybrid(microVM, ca, hIr) { (len, i, hElemIr) =>
        if (i > 0) print(" ")

        val hElem = ca.load(NOT_ATOMIC, hElemIr)

        val str = stringifyJSValue(ca, hElem)
        print(str)

        ca.deleteHandle(hElem)
      }

      println()
    }

    private def stringifyJSValue(ca: ClientAgent, hElem: Handle): String = {
      if (ca.tr64IsRef(hElem)) {

        val hTag = ca.tr64ToTag(hElem)
        val tag = ca.toInt(hTag, true).toInt

        val result = tag match {
          case JSTags.TAG_UNDEF => "undefined"
          case JSTags.TAG_NULL  => "null"
          case JSTags.TAG_FALSE => "false"
          case JSTags.TAG_TRUE  => "true"
          case JSTags.TAG_STRING => {
            val hRef = ca.tr64ToRef(hElem)
            val hRefStr = ca.refCast(hRef, "@js.string.r")
            val r = stringifyJSString(ca, hRefStr)
            ca.deleteHandles(hRef, hRefStr)
            r
          }
          case JSTags.TAG_CLOSURE => "closure"
          case _ => {
            throw new JSRuntimeException("Cannot print this kind of value. tag=" + tag)
          }
        }
        ca.deleteHandles(hTag)

        result
      } else if (ca.tr64IsFp(hElem)) {
        val hDouble = ca.tr64ToFp(hElem)
        val d = ca.toDouble(hDouble)
        ca.deleteHandle(hDouble)
        d.toString()
      } else {
        throw new JSRuntimeException("Cannot print a tr64 int")
      }
    }

    private def stringifyJSString(ca: ClientAgent, hStr: Handle): String = {
      val hIr = ca.getIRef(hStr)

      val sb = new StringBuilder()
      iterHybrid(microVM, ca, hIr) { (len, i, hElemIr) =>
        val hCh = ca.load(NOT_ATOMIC, hElemIr)
        val ch = ca.toInt(hCh, true).toChar
        sb.append(ch)

        ca.deleteHandle(hCh)
      }

      ca.deleteHandle(hIr)

      sb.toString()
    }

    private def jsTypeOf(ca: ClientAgent, hElem: Handle): String = {
      if (ca.tr64IsRef(hElem)) {
        val hTag = ca.tr64ToTag(hElem)
        val tag = ca.toInt(hTag, true).toInt

        val result = tag match {
          case JSTags.TAG_UNDEF   => "undefined"
          case JSTags.TAG_NULL    => "null"
          case JSTags.TAG_FALSE   => "boolean"
          case JSTags.TAG_TRUE    => "boolean"
          case JSTags.TAG_STRING  => "string"
          case JSTags.TAG_CLOSURE => "closure"
          case _                  => "unknown_" + tag
        }
        ca.deleteHandles(hTag)

        result
      } else if (ca.tr64IsFp(hElem)) {
        "number"
      } else {
        "tr64_int"
      }
    }

    private def printValueWithType(ca: ClientAgent, value: Handle, valueName: String) {
      println(valueWithType(ca, value, valueName))
    }

    private def valueWithType(ca: ClientAgent, value: Handle, valueName: String): String = {
      val ty = jsTypeOf(ca, value)
      val vs = stringifyJSValue(ca, value)
      s"${valueName}: ${ty} = ${vs}"
    }

    private def getJSType(ca: ClientAgent, value: Handle): JSTypes = {
      if (ca.tr64IsRef(value)) {
        val hTag = ca.tr64ToTag(value)
        val tag = ca.toInt(hTag, true).toInt

        val result = tag match {
          case JSTags.TAG_UNDEF   => JSTypes.UNDEF
          case JSTags.TAG_NULL    => JSTypes.NULL
          case JSTags.TAG_FALSE   => JSTypes.BOOLEAN
          case JSTags.TAG_TRUE    => JSTypes.BOOLEAN
          case JSTags.TAG_STRING  => JSTypes.STRING
          case JSTags.TAG_CLOSURE => JSTypes.CLOSURE
          case _ => {
            throw new JSRuntimeException("Unknown type encountered: tag = " + tag)
          }
        }
        ca.deleteHandles(hTag)

        result
      } else if (ca.tr64IsFp(value)) {
        JSTypes.DOUBLE
      } else {
        throw new JSRuntimeException("Unknown type encountered: a TR64 int")
      }
    }

    private def handleOptTrap(bei: BackEdgeInfo, ca: ClientAgent, stack: Handle, thread: Handle, watchPointID: Int): TrapHandlerResult = {
      println("Loop requesting for optimization.")
      val node = bei.node
      val jsFunc = bei.blFunc.jsFunc
      val source = jsFunc.source
      println(SourceHelper.richerMarker(source, node))

      // Dump keep-alive variables.
      val lvs = bei.blFunc.jsFunc.getAllLocalVars()
      val ka = ca.dumpKeepalives(stack, 0)

      val kaValues = new ArrayBuffer[Handle]()
      val varInfo = new HashMap[String, JSTypes]()

      // Find their types.
      for ((lv, h) <- (lvs zip ka)) {
        val vh = ca.load(NOT_ATOMIC, h)
        printValueWithType(ca, vh, lv)
        val jsType = getJSType(ca, vh)
        varInfo(lv) = jsType
        kaValues += vh
      }

      val osrInfo = new OSRInfo(node, varInfo.toMap)

      // Opt-compile the function with OSR information (where to continue, type of local variables).
      val optFunc = optimizingCompiler.compileFunction(jsFunc, Some(osrInfo), showHIR = true, showMuIR = true, optimize = true, eager = false)

      val optFuncMuName = optFunc.muFunc.muName
      val hOptFunc = ca.putFunction(optFuncMuName)

      val hArgListLen = ca.putInt("@i64", kaValues.length)

      val hArgList = ca.newHybrid("@js.paramList", hArgListLen)
      val hArgListIr = ca.getIRef(hArgList)
      val hArgListFix = ca.getFixedPartIRef(hArgListIr)
      iterHybrid(microVM, ca, hArgListIr, Some(kaValues.length)) { (len, i, hElemIr) =>
        ca.store(NOT_ATOMIC, hElemIr, kaValues(i.toInt))
      }

      // Do replacement
      ca.popFrame(stack)
      ca.pushFrame(stack, hOptFunc, Seq(hArgList))

      TrapRebindPassVoid(stack)
    }

    private def lowLevelStackTrace(ca: ClientAgent, stack: Handle, initialFrame: Int) {
      var curFrame = initialFrame
      var shouldContinue = true
      while (shouldContinue) {
        val curFuncVerID = ca.currentFuncVer(stack, curFrame)
        val curFuncVerName = nameOf(curFuncVerID)

        if (curFuncVerName equals "@js.launcher.v1") {
          shouldContinue = false
        } else {
          val curInstID = ca.currentInstruction(stack, curFrame)
          val curInstName = nameOf(curInstID)

          println("%s: %s".format(curFuncVerName, curInstName))

          curFrame += 1
        }
      }
    }

    private def printMarker(ri: JSRuntimeInstInfo[Node]): Unit = {
      val marker = SourceHelper.richerMarker(ri.blFunc.jsFunc.source, ri.node)
      println(marker)
    }
  }

  microVM.trapManager.trapHandler = trapHandler

  /**
   * Compiling a JS script using the baseline compiler and execute it. OSR can be enabled so the optimising compiler
   * will optimise hot loops and hot functions when executed often.
   *
   * @param script The source code.
   * @param execute Execute. If false, only print the Mu IR code, but does not execute.
   * @param allowOsr Put profiling in the Mu IR code to allow the opt compiler optimise it.
   * @param forceOSROnCall Perform OSR in the entry block of a function every time the function is called. This is used
   * to test the type inferrer.
   */
  def execute(script: String, execute: Boolean = true, allowOsr: Boolean = false,
              forceOSROnCall: Boolean = false): Unit = {
    val bu = baselineCompiler.compileScript(script, allowOsr = allowOsr, forceOSROnCall = forceOSROnCall)
    val bdlText = bu.muBundle.toMuIR()
    println(bdlText)

    if (execute) {
      val topLevel = bu.topLevelFunc
      val topLevelMuName = topLevel.muFunc.muName

      val hLauncher = ca.putFunction("@js.launcher")
      val hTopLevel = ca.putFunction(topLevelMuName)
      val hStack = ca.newStack(hLauncher, Seq(hTopLevel))

      val hThread = ca.newThread(hStack)

      microVM.threadStackManager.joinAll()
    }
  }

  /**
   * Compile a JS source code direcly using the opt compiler and execute it.
   *
   * @param script The source code
   * @param optimize Run optimisation passes.
   * @param showHIR Print the HIR cfg.
   * @param showMuCode Print the Mu IR code generated by the optimiser.
   * @param execute Execute the resulting program. If false, only compile.
   */
  def executeWithOpt(script: String, optimize: Boolean = true, showHIR: Boolean = true, showMuCode: Boolean = true,
                     execute: Boolean = true): Unit = {
    val bu = baselineCompiler.compileScript(script, allowOsr = false)
    val bdlText = bu.muBundle.toMuIR()
    println(bdlText)

    logger.debug("Starting opt compiling...")

    for (fn <- bu.funcs) {
      val jsFunc = fn.jsFunc
      optimizingCompiler.compileFunction(jsFunc, showHIR = showHIR, optimize = optimize, eager = true)

      val optFunc = jsFunc.optFunc.get
      val optMuCode = optFunc.muFunc.toMuIR()

      if (showMuCode) {
        println(optMuCode)
      }
    }

    if (execute) {
      val topLevelJSFunc = bu.topLevelFunc.jsFunc
      val topLevelOptMuName = topLevelJSFunc.optFunc.get.muFunc.muName

      val hLauncher = ca.putFunction("@js.launcher")
      val hTopLevel = ca.putFunction(topLevelOptMuName)
      val hStack = ca.newStack(hLauncher, Seq(hTopLevel))

      val hThread = ca.newThread(hStack)

      microVM.threadStackManager.joinAll()
    }
  }
}

class JSClientException(msg: String = null, cause: Throwable = null) extends RuntimeException(msg, cause)
class JSRuntimeException(msg: String = null, cause: Throwable = null) extends JSClientException(msg, cause)
