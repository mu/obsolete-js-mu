package org.microvm.jsmu.client

import uvm.refimpl._
import uvm.ssavariables.MemoryOrder

object MicroVMHelpers {
  implicit class ClientAgentHelper(ca: ClientAgent) {
    def deleteHandles(hs: Handle*) {
      for (h <- hs) {
        ca.deleteHandle(h)
      }
    }
  }

  /**
   * Iterate through a hybrid with an @i64 fixed part.
   * <p>
   * In the callback,
   * <ol>
   * <li>The first parameter is the actual value of the fixed part.
   * <li>The second parameter is the index.
   * <li>The third parameter is a handle of an iref of an index-th element of the var part.
   * </ol>
   * This function will delete all handles it creates, but does not delete the handle passed in.
   */
  def iterHybrid(microVM: MicroVM, ca: ClientAgent, hHybridIr: Handle, maybeKnownLength: Option[Long] = None)(f: (Long, Long, Handle) => Unit) {
    val len = maybeKnownLength match {
      case None => {
        val hFix = ca.getFixedPartIRef(hHybridIr)
        val hLen = ca.load(MemoryOrder.NOT_ATOMIC, hFix)
        val lenValue = ca.toInt(hLen, true).toLong
        ca.deleteHandles(hFix, hLen)
        lenValue
      }
      case Some(lenValue) => lenValue
    }

    val hVar = ca.getVarPartIRef(hHybridIr)
    for (i <- 0L until len) {
      val hIndex = ca.putInt(microVM.idOf("@i64"), i)
      val hElemIr = ca.shiftIRef(hVar, hIndex)
      f(len, i, hElemIr)
      ca.deleteHandles(hIndex, hElemIr)
    }

    ca.deleteHandles(hVar)
  }

}
