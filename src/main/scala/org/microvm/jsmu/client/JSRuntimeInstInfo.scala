package org.microvm.jsmu.client

import jdk.nashorn.internal.ir._
import org.microvm.jsmu.compiler.baseline.BaselineFunction

case class JSRuntimeInstInfo[+T <: Node](blFunc: BaselineFunction, kind: InfoKind[T]) {
  def node: T = kind.node
}

abstract class InfoKind[+T <: Node] {
  def node: T
}

/**
 * A CALL to @js.error because the callee is not a closure during calling a JS function.
 */
case class CalleeNotClosure(node: CallNode) extends InfoKind[CallNode]

/**
 * A CALL to @js.??? for doing a binary operation.
 */
case class BinaryOp(node: BinaryNode) extends InfoKind[BinaryNode]

/**
 * A CALL to @js.??? for doing a unary operation.
 */
case class UnaryOp(node: UnaryNode) extends InfoKind[UnaryNode]

/**
 * A CALL to @js.truth for testing the truth of a jsvalue.
 */
case class TruthTest(node: Expression) extends InfoKind[Expression]

/**
 * A CALL to @js.add.gen for unary inc/dec (i++, i--, ++i, --i).
 */
case class UnaryIncDec(node: UnaryNode) extends InfoKind[UnaryNode]

/**
 * A CALL to @js.prop_put for property put (obj[prop] = val, or obj[prop]++).
 */
case class PropPut(node: Node) extends InfoKind[Node]

/**
 * Any other internal CALL instructions generated for other statements. This kind of instructions
 * should never cause errors for erroneous user program
 *
 * @param desc: A human-readable description of this operation, for debugging.
 */
case class InternalOp(desc: String, node: Node) extends InfoKind[Node]

