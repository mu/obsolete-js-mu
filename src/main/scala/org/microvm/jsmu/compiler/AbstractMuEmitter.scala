package org.microvm.jsmu.compiler

import org.microvm.jsmu.client.JSClient
import org.microvm.jsmu.muir.MuIRFunctionBuilder
import org.microvm.jsmu.client.JSFunction

trait AbstractMuEmitter extends MuIRFunctionBuilder {
  def jsClient: JSClient

  def constLong(v: Long): String = {
    jsClient.constantPool.getLongConstant(v)
  }

  def constDouble(d: Double): String = {
    jsClient.constantPool.getDoubleConstant(d)
  }

  def constString(s: String): String = {
    jsClient.constantPool.getStringConstant(s)
  }

  def constBaselineClosure(jsFunc: JSFunction): String = {
    jsClient.constantPool.getBaselineClosureConstant(jsFunc)
  }
  
  def constOptimizedClosure(jsFunc: JSFunction): String = {
    jsClient.constantPool.getOptimizedClosureConstant(jsFunc)
  }

  val CONTINUABLE = true
  val UNCONTINUABLE = false

  def ifThenElse(nameHint: String)(ifClause: => String)(thenClause: => Boolean)(elseClause: => Boolean): Unit = {
    val prevBB = curBB
    val bbThen = newBB(nameHint + "_then")
    val bbElse = newBB(nameHint + "_else")
    val bbEnd = newBB(nameHint + "_end")

    val ifCond = ifClause
    emit(nameHint + "_br2", s"BRANCH2 ${ifCond} ${bbThen.muName} ${bbElse.muName}")

    use(bbThen)
    val thenContinuable = thenClause
    if (thenContinuable) {
      emit(nameHint + "_thenEnd", s"BRANCH ${bbEnd.muName}")
    } else {
      emit("unreachable", s"THROW @NULL")
    }

    use(bbElse)
    val elseContinuable = elseClause
    if (elseContinuable) {
      emit(nameHint + "+elseEnd", s"BRANCH ${bbEnd.muName}")
    } else {
      emit("unreachable", s"THROW @NULL")
    }
    
    use(bbEnd)
  }
}
