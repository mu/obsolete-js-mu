package org.microvm.jsmu.compiler.baseline

import java.io.PrintWriter

import scala.collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer

import org.microvm.jsmu.client._
import org.microvm.jsmu.compiler._
import org.microvm.jsmu.muir._
import org.microvm.jsmu.muir.ImplicitConversions._
import org.slf4j.LoggerFactory

import com.typesafe.scalalogging.Logger

import jdk.nashorn.internal.ir._
import jdk.nashorn.internal.ir.LiteralNode._
import jdk.nashorn.internal.parser._
import jdk.nashorn.internal.runtime._
import jdk.nashorn.internal.runtime.options.Options

object BaselineCompiler {
  val logger: Logger = Logger(LoggerFactory.getLogger(getClass.getName))
}

class BaselineCompiler(val jsClient: JSClient) {
  val funcNamer = new MuNamer("@func")

  object globalScope extends Scope(None) {
    // varMap is just a cache of the globalPool.

    override def getWithParent(name: String): Option[LValue] = {
      Some(varMap.getOrElseUpdate(name, {
        val newGlobalMuName = jsClient.globalPool.getGlobalCell(name)
        JSGlobalVar(newGlobalMuName)
      }))
    }
  }

  def compileScript(code: String, allowOsr: Boolean = false, forceOSROnCall: Boolean = false): BaselineUnit = {
    val bsc = new BaselineScriptCompiler(this, code, allowOsr = allowOsr, forceOSROnCall = forceOSROnCall)

    bsc.unit
  }
}

class BaselineScriptCompiler(val baselineCompiler: BaselineCompiler, val script: String, val allowOsr: Boolean,
                             val forceOSROnCall: Boolean) {
  val unit = new BaselineUnit(script)
  val jsClient = baselineCompiler.jsClient

  val (source, funcNode) = {
    val se = new ScriptEnvironment(
      new Options("nashorn"), new PrintWriter(System.out), new PrintWriter(System.err))

    val src = Source.sourceFor("the_source_code", script)
    val em = new ErrorManager()

    val p = new Parser(se, src, em)
    val funcNode = p.parse()

    if (em.hasErrors()) {
      throw new BaselineCompileError("Script contains syntax errors.")
    }

    (src, funcNode)
  }

  /**
   * Baseline-compile a FunctionNode. It also adds a JSFunction to the global JSFunctionManager.
   */
  def compileFunc(funcNode: FunctionNode, nameHint: String): JSFunction = {
    val jsFunc = new JSFunction(nameHint, source, funcNode)
    jsClient.funcManager.jsFunctions(funcNode) = jsFunc

    val b = new BaselineFunctionBuilder(this, jsFunc)
    val func = b.blFunc

    unit.funcs += func
    unit.muBundle.funcs += func.muFunc

    jsFunc.blFunc = Some(func)

    return jsFunc
  }

  val toplevelFunc = compileFunc(funcNode, "toplevel").blFunc.get
  unit.topLevelFunc = toplevelFunc

}

class BaselineFunctionBuilder(val baselineScriptCompiler: BaselineScriptCompiler,
                              val jsFunc: JSFunction) extends {
  val source = jsFunc.source
  val funcNode = jsFunc.funcNode
  val nameHint = jsFunc.nameHint
  val baselineCompiler = baselineScriptCompiler.baselineCompiler
  val funcMuName = baselineCompiler.funcNamer.next(nameHint)
  val paramListMuName = funcMuName % "params"
} with MuIRFunctionBuilder(funcMuName, "@js.userfunc.sig", Seq(paramListMuName))
    with AbstractMuEmitter {

  import BaselineCompiler._

  val jsClient = baselineCompiler.jsClient

  val jsParams: Seq[String] = funcNode.getParameters.map(_.getName).toSeq

  val blFunc = new BaselineFunction(jsFunc, func)

  /** A list of Mu names of ALLOCAs for local variables */
  val lazyAllocas = new ArrayBuffer[String]()

  val profCounterMuName = jsClient.globalPool.getProfCounterFor(funcMuName)

  val allowOsr = baselineScriptCompiler.allowOsr
  val forceOSROnCall = baselineScriptCompiler.forceOSROnCall

  build()

  def build() {
    implicit val funcScope = new Scope(Some(baselineCompiler.globalScope))

    makeParams()

    if (forceOSROnCall) {
      // This effectively makes the baseline function a stub for generating opt functions.
      // For practical reasons, I still need to execute the rest parts because all other passes require a list of
      // local variables which is built by traversion the AST.
      makeEntryTrap()
    }

    visitBlock(funcNode.getBody)

    emitEmptyReturn()

    makeLocalVarAllocas()

    populateOptBlocks()

    val funcMuIR = func.toMuIR()
    try {
      jsClient.ca.loadBundle(funcMuIR)
    } catch {
      case e: Exception => {
        println("Error occured when loading function bundle. The Mu IR was:")
        println(funcMuIR)
        throw e
      }
    }
  }

  def makeParams()(implicit scope: Scope) {
    val allocas = jsParams.map { param =>
      val allocaMuName = newAlloca(param)
      scope.varMap(param) = MuLoadable(allocaMuName)
      allocaMuName
    }

    // Allocas will be pre-pended to the %entry block after compiling the whole function, so we can start generating
    // initialisation instructions now.
    val plIRMuName = emit(s"paramsIR", s"GETIREF <@js.paramList> ${paramListMuName}")
    val plFixMuName = emit(s"paramsFix", s"GETFIXEDPARTIREF <@js.paramList> ${plIRMuName}")
    val plLenMuName = emit(s"paramsLen", s"LOAD <@i64> ${plFixMuName}")
    var indexMuName = "@I64_0"
    allocas.zipWithIndex.foreach {
      case (alloca, i) =>
        val paramValMuName = emit(s"getParam${i}", s"CALL <@js.get_param.sig> @js.get_param (${paramListMuName} ${plLenMuName} ${indexMuName})")
        emit(s"storeParam${i}", s"STORE <@js.value> ${alloca} ${paramValMuName}")
        indexMuName = emit(s"incIndexTo${i + 1}", s"ADD <@i64> ${indexMuName} @I64_1")
    }
  }

  def makeEntryTrap()(implicit scope: Scope) {
    val entryTrap = newBB("entryTrap")
    val afterEntry = newBB("afterEntry")
    val alwaysTrue = emit(s"alwaysTrue", s"EQ <@i64> @I64_1 @I64_1")
    emit(s"entryTrapBr", s"BRANCH2 ${alwaysTrue} ${entryTrap.muName} ${afterEntry.muName}")

    val backEdgeInfo = new BackEdgeInfo(blFunc, funcNode, curBB, entryTrap)
    blFunc.backEdgeInfos += backEdgeInfo
    
    use(afterEntry)
  }

  def makeLocalVarAllocas() {
    val allocas = lazyAllocas.map { laMuName =>
      val inst = new MuInst(laMuName, "ALLOCA <@js.value>")
      inst
    }

    entry.insts.prependAll(allocas)
  }

  def populateOptBlocks()(implicit scope: Scope) {
    for (bei <- blFunc.backEdgeInfos) {
      val trapBB = bei.trapBB
      use(trapBB)

      val trapMuName = emit("optTrap", s"TRAP <@void> KEEPALIVE (${lazyAllocas.mkString(" ")})")

      bei.trapMuName = trapMuName
      jsClient.backEdgeInfoMap(trapMuName) = bei
    }
  }

  def visitBlock(block: Block)(implicit scope: Scope) {
    for (stmt <- block.getStatements) {
      visitStatement(stmt)
    }
  }

  def visitStatement(stmt: Statement)(implicit scope: Scope): Unit = stmt match {
    case s: ExpressionStatement => visitExpressionStatement(s)
    case s: VarNode             => visitVarNode(s)
    case s: IfNode              => visitIfNode(s)
    case s: WhileNode           => visitWhileNode(s)
    case s: ForNode             => visitForNode(s)
    case s: ReturnNode          => visitReturnNode(s)
    case _                      => throw new NotImplementedError("Other statements %s: %s".format(stmt, stmt.getClass) inCtx stmt)
  }

  def visitExpressionStatement(stmt: ExpressionStatement)(implicit scope: Scope) {
    val expr = stmt.getExpression

    val mnRv = visitExpression(expr)
  }

  def visitVarNode(stmt: VarNode)(implicit scope: Scope) {
    val lhsName = stmt.getName.getName
    if (scope.varMap.contains(lhsName)) {
      throw new UnimplementedFeatureError(("Variable shadowing " + lhsName) inCtx stmt)
    }

    blFunc.localVars += lhsName
    val lhsAllocaMuName = newAlloca(lhsName)
    scope.varMap(lhsName) = MuLoadable(lhsAllocaMuName)

    val maybeRhsExpr = Option(stmt.getAssignmentSource)
    maybeRhsExpr match {
      case None =>
      case Some(rhsExpr) => {
        val rhsRv = visitExpression(rhsExpr)

        val rhsMuName = prepareRValue(rhsRv)
        emit("storeVar", s"STORE <@js.value> ${lhsAllocaMuName} ${rhsMuName}")
      }
    }
  }

  def newAlloca(varName: String): String = {
    val muName = instNamer.next("var_" + varName)
    lazyAllocas += muName
    muName
  }

  def visitIfNode(ifNode: IfNode)(implicit scope: Scope): Unit = {
    val tst = ifNode.getTest
    val pas = ifNode.getPass
    val maybeFal = Option(ifNode.getFail)

    val tstRValue = visitExpression(tst)

    val tstMuName = prepareRValue(tstRValue)
    val truthMuName = emit("truthTest", s"CALL <@js.truth.sig> @js.truth (${tstMuName})")
    registerRuntimeInstInfo(truthMuName, TruthTest(tst))
    val truthI1 = emit("truthI1", s"EQ <@i64> ${truthMuName} @I64_1")

    val bbPass = newBB("ifPass")
    val maybeBBFail = if (maybeFal.nonEmpty) Some(newBB("ifFail")) else None
    val bbEnd = newBB("ifEnd")

    if (maybeFal.nonEmpty) {
      emit("ifBranchWithElse", s"BRANCH2 ${truthI1} ${bbPass.muName} ${maybeBBFail.get.muName}")
    } else {
      emit("ifBranchNoElse", s"BRANCH2 ${truthI1} ${bbPass.muName} ${bbEnd.muName}")
    }

    use(bbPass)
    visitBlock(pas)
    emit("endIfFromPass", s"BRANCH ${bbEnd.muName}")

    if (maybeBBFail.nonEmpty) {
      use(maybeBBFail.get)
      visitBlock(maybeFal.get)
      emit("endIfFromFail", s"BRANCH ${bbEnd.muName}")
    }

    use(bbEnd)
  }

  def visitWhileNode(whileNode: WhileNode)(implicit scope: Scope): Unit = {
    if (whileNode.isDoWhile()) {
      throw new UnimplementedFeatureError(("Do-while loop") inCtx whileNode)
    }

    val tst = whileNode.getTest.getExpression
    val body = whileNode.getBody
    val beforeBack = () => {}
    generalLoop(whileNode, "while", Some(tst), body, beforeBack)
  }

  def visitForNode(forNode: ForNode)(implicit scope: Scope): Unit = {
    if (forNode.isForIn()) {
      throw new UnimplementedFeatureError(("For-in loop") inCtx forNode)
    }

    val maybeInit = Option(forNode.getInit)
    val maybeTst = Option(forNode.getTest).map(_.getExpression)
    val body = forNode.getBody
    val maybeNext = Option(forNode.getModify).map(_.getExpression)
    val beforeBack = () => { maybeNext foreach { next => visitExpression(next) } }

    maybeInit.foreach(init => visitExpression(init))
    generalLoop(forNode, "for", maybeTst, body, beforeBack)
  }

  def generalLoop(node: Node, kind: String, maybeTst: Option[Expression], body: Block, beforeBack: () => Unit)(implicit scope: Scope): Unit = {
    val bbHead = newBB(s"${kind}Head")
    emit(s"${kind}EnterHead", s"BRANCH ${bbHead.muName}")
    use(bbHead)

    // loop backedge counting (not really back edges. It is in the header.)
    maybeEmitProfilingPoint(node, kind)

    val bbBody = newBB(s"${kind}Body")
    val bbEnd = newBB(s"${kind}End")

    maybeTst match {
      case Some(tst) => {
        val tstRValue = visitExpression(tst)

        val tstMuName = prepareRValue(tstRValue)
        val truthMuName = emit("truthTest", s"CALL <@js.truth.sig> @js.truth (${tstMuName})")
        registerRuntimeInstInfo(truthMuName, TruthTest(tst))
        val truthI1 = emit("truthI1", s"EQ <@i64> ${truthMuName} @I64_1")
        emit(s"${kind}Branch", s"BRANCH2 ${truthI1} ${bbBody.muName} ${bbEnd.muName}")
      }
      case None => {
        emit(s"${kind}BranchUncond", s"BRANCH ${bbBody.muName}")
      }
    }

    use(bbBody)
    visitBlock(body)

    beforeBack()

    emit(s"${kind}BackToHead", s"BRANCH ${bbHead.muName}")

    use(bbEnd)
  }

  def visitReturnNode(returnNode: ReturnNode)(implicit scope: Scope): Unit = {
    if (returnNode.hasExpression()) {
      val expr = returnNode.getExpression
      val exprRValue = visitExpression(expr)
      val exprMuName = prepareRValue(exprRValue)
      emit("return", s"RET <@js.value> ${exprMuName}")
    } else {
      emitEmptyReturn()
    }
  }

  def visitExpression(expr: Expression)(implicit scope: Scope): RValue = expr match {
    case e: LiteralNode[_] => visitLiteral(e)
    case e: CallNode       => visitCall(e)
    case e: IdentNode      => visitIdent(e)
    case e: BinaryNode     => visitBinary(e)
    case e: UnaryNode      => visitUnary(e)
    case e: FunctionNode   => visitFunction(e)
    case e: IndexNode      => visitIndex(e)
    case _                 => throw new UnimplementedFeatureError("Other expression %s: %s".format(expr, expr.getClass) inCtx expr)
  }

  def visitLiteral(ln: LiteralNode[_])(implicit scope: Scope): RValue = {
    if (ln.isNull()) {
      emitNull()
    } else if (ln.isNumeric()) {
      emitDouble(ln.getNumber())
    } else if (ln.isString()) {
      emitString(ln.getString())
    } else if (ln.isInstanceOf[ArrayLiteralNode]) {
      val al = ln.asInstanceOf[ArrayLiteralNode]
      val values = al.getValue

      val valMuNames = values map { e =>
        val rv = visitExpression(e)
        val mn = prepareRValue(rv)
        mn
      }

      val len = values.length
      val lenLit = constLong(len)
      val aryMuName = emit("arrayLitNew", s"CALL <@js.array_new_priv.sig> @js.array_new_priv (${lenLit})")

      var ind = "@I64_0"
      for ((mn, i) <- valMuNames.zipWithIndex) {
        emit("arrayLitElem" + i, s"CALL <@js.array_put.sig> @js.array_put (${aryMuName} ${ind} ${mn})")
        val ind2 = emit("incIndTo" + (i + 1), s"ADD <@i64> ${ind} @I64_1")
        ind = ind2
      }

      val resMuName = emit("arrayLitToTR", s"COMMINST @uvm.tr64.from_ref (${aryMuName} @js.array.tag)")
      ValueInMuVar(resMuName)
    } else {
      val obj = ln.getObject()
      obj match {
        case b: java.lang.Boolean => {
          if (b == true) {
            emitTrue()
          } else {
            emitFalse()
          }
        }
        case _ => throw new UnimplementedFeatureError(("Other constants " + ln) inCtx ln)
      }
    }
  }

  def visitCall(cn: CallNode)(implicit scope: Scope): RValue = {
    val calleeExpr = cn.getFunction()

    calleeExpr match {
      case i: IdentNode => {
        if (i.getName == "Array") {
          return emitArrayNew(cn)
        }
      }
      case _ =>
    }

    val callee = visitExpression(calleeExpr)
    val args = cn.getArgs().map(arg => visitExpression(arg))

    val calleeMN = prepareRValue(callee)
    val bbNotClosure = newBB("notClosure")
    val bbIsRef = newBB("isRef")
    val bbIsClosure = newBB("isClosure")

    val calleeIsRef = emit("calleeIsRef", s"COMMINST @uvm.tr64.is_ref (${calleeMN})")
    emit("brIsRef", s"BRANCH2 ${calleeIsRef} ${bbIsRef.muName} ${bbNotClosure.muName}")

    use(bbIsRef)
    val calleeTag = emit("getTagFromCallee", s"COMMINST @uvm.tr64.to_tag (${calleeMN})")
    val calleeTagIsClosure = emit("tagIsClosure", s"EQ <@i6> ${calleeTag} @js.closure.tag")
    emit("brIsClosure", s"BRANCH2 ${calleeTagIsClosure} ${bbIsClosure.muName} ${bbNotClosure.muName}")

    use(bbNotClosure)
    val errorCall = emit("callError", s"CALL <@js.error.sig> @js.error () KEEPALIVE (${calleeMN})")
    registerRuntimeInstInfo(errorCall, CalleeNotClosure(cn))
    emit("unreachable", s"THROW @NULL")
    use(bbIsClosure)
    val refClosureVoid = emit("toRef", s"COMMINST @uvm.tr64.to_ref (${calleeMN})")
    val refClosure = emit("toRefClosure", s"REFCAST <@refvoid @js.closure.r> ${refClosureVoid}")
    val iRefClosure = emit("toIRefClosure", s"GETIREF <@js.closure> ${refClosure}")
    val iRefFunc = emit("toIRefFunc", s"GETFIELDIREF <@js.closure 0> ${iRefClosure}")
    val funcMN = emit("loadFuncFromClosure", s"LOAD <@js.userfunc> ${iRefFunc}")

    val argsMN = args.map(arg => prepareRValue(arg))

    val nArgs = args.length
    val nArgsMN = constLong(nArgs)

    val argList = emit("newArgList", s"NEWHYBRID <@js.paramList @i64> ${nArgsMN}")
    val argListIr = emit("argListIr", s"GETIREF <@js.paramList> ${argList}")
    val argListFixIr = emit("argListFixIr", s"GETFIXEDPARTIREF <@js.paramList> ${argListIr}")
    emit(s"putParamLen", s"STORE <@i64> ${argListFixIr} ${nArgsMN}")

    val argListVarIr = emit("argListVarIr", s"GETVARPARTIREF <@js.paramList> ${argListIr}")
    val argIndex = "@I64_0"

    var curIndexVal = argIndex
    for ((arg, i) <- argsMN.zipWithIndex) {
      val argElem = emit(s"argElem${i}", s"SHIFTIREF <@js.value @i64> ${argListVarIr} ${curIndexVal}")
      emit(s"putArg${i}", s"STORE <@js.value> ${argElem} ${arg}")
      curIndexVal = emit(s"incArgTo${i + 1}", s"ADD <@i64> ${curIndexVal} @I64_1")
    }

    val call = emit("call", s"CALL <@js.userfunc.sig> ${funcMN} (${argList})")

    ValueInMuVar(call)
  }

  def visitIdent(idn: IdentNode)(implicit scope: Scope): LValue = {
    scope.getWithParent(idn.getName).getOrElse {
      throw new BaselineCompileError(s"Undefined variable ${idn.getName}, but this should not happen. Undefined vars are global vars." inCtx idn)
    }
  }

  def visitBinary(bn: BinaryNode)(implicit scope: Scope): RValue = {
    val tokTy = bn.tokenType()

    tokTy match {
      case TokenType.ASSIGN    => visitAssignment(bn)
      case TokenType.ADD       => visitBinaryEager(bn, "add", "@js.add.gen", flip = false, negate = false)
      case TokenType.SUB       => visitBinaryEager(bn, "sub", "@js.sub.gen", flip = false, negate = false)
      case TokenType.MUL       => visitBinaryEager(bn, "mul", "@js.mul.gen", flip = false, negate = false)
      case TokenType.DIV       => visitBinaryEager(bn, "div", "@js.div.gen", flip = false, negate = false)
      case TokenType.EQ        => visitBinaryEager(bn, "eqa", "@js.eqs.gen", flip = false, negate = false)
      case TokenType.EQ_STRICT => visitBinaryEager(bn, "eqs", "@js.eqs.gen", flip = false, negate = false)
      case TokenType.NE        => visitBinaryEager(bn, "nea", "@js.eqs.gen", flip = false, negate = true)
      case TokenType.NE_STRICT => visitBinaryEager(bn, "nes", "@js.eqs.gen", flip = false, negate = true)
      case TokenType.LT        => visitBinaryEager(bn, "lt", "@js.lt.gen", flip = false, negate = false)
      case TokenType.GT        => visitBinaryEager(bn, "gt", "@js.lt.gen", flip = true, negate = false)
      case TokenType.LE        => visitBinaryEager(bn, "le", "@js.lt.gen", flip = true, negate = true)
      case TokenType.GE        => visitBinaryEager(bn, "ge", "@js.lt.gen", flip = false, negate = true)
      case _                   => throw new UnimplementedFeatureError(("Other binary operator " + tokTy) inCtx bn)
    }
  }

  def visitAssignment(bn: BinaryNode)(implicit scope: Scope): RValue = {
    val lhs = bn.getAssignmentDest
    val lhsRValue = visitExpression(lhs)

    lhsRValue match {
      case lhsLValue: LValue => {

        val rhs = bn.getAssignmentSource
        val rhsRValue = visitExpression(rhs)

        emitAssign(bn, "assignment", lhsLValue, rhsRValue)

        rhsRValue
      }
      case _ => {
        throw new BaselineCompileError(s"Assign to non-lvalue." inCtx lhs)
      }
    }
  }

  def visitBinaryEager(bn: BinaryNode, nameHint: String, muInst: String, flip: Boolean, negate: Boolean)(implicit scope: Scope): RValue = {
    val (realLhs, realRhs) = if (flip) (bn.rhs(), bn.lhs()) else (bn.lhs(), bn.rhs())
    val lhsRValue = visitExpression(realLhs)
    val lhsMuName = prepareRValue(lhsRValue)
    val rhsRValue = visitExpression(realRhs)
    val rhsMuName = prepareRValue(rhsRValue)

    val resultMuName = emit(nameHint, s"CALL <@js.binop.gen.sig> ${muInst} (${lhsMuName} ${rhsMuName})")
    registerRuntimeInstInfo(resultMuName, BinaryOp(bn))

    val realResultMuName = if (negate) {
      val r = emit(nameHint, s"CALL <@js.unop.gen.sig> @js.not.gen (${resultMuName})")
      registerRuntimeInstInfo(r, InternalOp("The (!) operator for <= or >=", bn))
      r
    } else {
      resultMuName
    }

    ValueInMuVar(realResultMuName)
  }

  def visitUnary(un: UnaryNode)(implicit scope: Scope): RValue = {
    val tokTy = un.tokenType()

    tokTy match {
      case TokenType.NOT        => visitUnaryEager(un, "not", "@js.not.gen")
      case TokenType.SUB        => visitUnaryEager(un, "neg", "@js.neg.gen")
      case TokenType.INCPREFIX  => visitUnaryIncDec(un, isInc = true, isPrefix = true)
      case TokenType.DECPREFIX  => visitUnaryIncDec(un, isInc = false, isPrefix = true)
      case TokenType.INCPOSTFIX => visitUnaryIncDec(un, isInc = true, isPrefix = false)
      case TokenType.DECPOSTFIX => visitUnaryIncDec(un, isInc = false, isPrefix = false)
      case _                    => throw new UnimplementedFeatureError(("Other unary operator " + tokTy) inCtx un)
    }
  }

  def visitUnaryEager(un: UnaryNode, nameHint: String, muInst: String)(implicit scope: Scope): RValue = {
    val opnd = un.getExpression()
    val opndRValue = visitExpression(opnd)
    val opndMuName = prepareRValue(opndRValue)

    val resultMuName = emit(nameHint, s"CALL <@js.unop.gen.sig> ${muInst} (${opndMuName})")

    registerRuntimeInstInfo(resultMuName, UnaryOp(un))
    ValueInMuVar(resultMuName)
  }

  def visitUnaryIncDec(un: UnaryNode, isInc: Boolean, isPrefix: Boolean)(implicit scope: Scope): RValue = {
    val opnd = un.getExpression()
    val opndRValue = visitExpression(opnd)
    opndRValue match {
      case opndLValue: LValue => {
        val opndRVMuName = prepareRValue(opndLValue)
        val rhsRValue = if (isInc) emitDouble(1.0) else emitDouble(-1.0)
        val rhsMuName = prepareRValue(rhsRValue)
        val resultMuName = emit("incDec", s"CALL <@js.binop.gen.sig> @js.add.gen (${opndRVMuName} ${rhsMuName})")
        registerRuntimeInstInfo(resultMuName, UnaryIncDec(un))

        emitAssign(un, "incDecStore", opndLValue, ValueInMuVar(resultMuName))

        ValueInMuVar(if (isPrefix) resultMuName else opndRVMuName)
      }
      case _ => {
        throw new BaselineCompileError(s"Inc/dec with non-lvalue." inCtx un)
      }
    }
  }

  def visitFunction(fn: FunctionNode)(implicit scope: Scope): RValue = {
    val nameHint = fn.getName

    val jsFunc = baselineScriptCompiler.compileFunc(fn, nameHint)

    val closureRValue = emitBaselineClosure(jsFunc)
    closureRValue
  }

  def visitIndex(ino: IndexNode)(implicit scope: Scope): LValue = {
    val base = ino.getBase
    val index = ino.getIndex

    val baseRValue = visitExpression(base)
    val indexRValue = visitExpression(index)

    PropAccess(baseRValue, indexRValue)
  }

  def emitUndef(): RValue = {
    val v = emit("emitUndef", s"COMMINST @uvm.tr64.from_ref (@NULL @js.undef.tag)")
    ValueInMuVar(v)
  }

  def emitNull(): RValue = {
    val v = emit("emitNull", s"COMMINST @uvm.tr64.from_ref (@NULL @js.null.tag)")
    ValueInMuVar(v)
  }

  def emitFalse(): RValue = {
    val v = emit("emitFalse", s"COMMINST @uvm.tr64.from_ref (@NULL @js.false.tag)")
    ValueInMuVar(v)
  }

  def emitTrue(): RValue = {
    val v = emit("emitTrue", s"COMMINST @uvm.tr64.from_ref (@NULL @js.true.tag)")
    ValueInMuVar(v)
  }

  def emitDouble(num: Double): RValue = {
    val constMN = jsClient.constantPool.getDoubleConstant(num)

    val v = emit("emitDouble", s"COMMINST @uvm.tr64.from_fp (${constMN})")
    ValueInMuVar(v)
  }

  def emitString(str: String): RValue = {
    val constGlobal = jsClient.constantPool.getStringConstant(str)

    logger.debug("constGlobal = '%s' for string '%s'".format(constGlobal, str))
    MuLoadable(constGlobal)
  }

  def emitBaselineClosure(jsFunc: JSFunction): RValue = {
    val closureMuName = constBaselineClosure(jsFunc)
    MuLoadable(closureMuName)
  }

  def emitEmptyReturn() {
    val undef = emitUndef()
    val undefMN = prepareRValue(undef)
    emit("emptyReturn", s"RET <@js.value> ${undefMN}")
  }

  def emitAssign(node: Node, nameHint: String, lhs: LValue, rhs: RValue): Unit = {
    lhs match {
      case MuLoadable(locMuName) => {
        val valMuName = prepareRValue(rhs)
        emitDirectAssign(nameHint, locMuName, valMuName)
      }
      case JSGlobalVar(locMuName) => {
        val valMuName = prepareRValue(rhs)
        emitDirectAssign(nameHint, locMuName, valMuName)
      }
      case PropAccess(obj, prop) => {
        val objMuName = prepareRValue(obj)
        val propMuName = prepareRValue(prop)
        val valMuName = prepareRValue(rhs)
        emitPropPut(node, nameHint, objMuName, propMuName, valMuName)
      }
    }
  }

  def emitDirectAssign(nameHint: String, locMuName: String, valMuName: String): Unit = {
    emit(nameHint, s"STORE <@js.value> ${locMuName} ${valMuName}")
  }

  def emitPropPut(node: Node, nameHint: String, objMuName: String, propMuName: String, valMuName: String): Unit = {
    val propPut = emit(nameHint, s"CALL <@js.prop_put.sig> @js.prop_put (${objMuName} ${propMuName} ${valMuName})")
    registerRuntimeInstInfo(propPut, PropPut(node))
  }

  /**
   * The Array(n) call.
   */
  def emitArrayNew(cn: CallNode)(implicit scope: Scope): RValue = {
    val args = cn.getArgs()
    if (args.size != 1) {
      throw new UnimplementedFeatureError("Currently the Array() call only accepts the length argument." inCtx cn)
    }

    val arg = args(0)
    val argRValue = visitExpression(arg)
    val argMuName = prepareRValue(argRValue)

    val resMuName = emit("arrayNew", s"CALL <@js.array_new.sig> @js.array_new (${argMuName})")
    ValueInMuVar(resMuName)
  }

  /**
   * Make the RValue available as a Mu variable so it can be used.
   * <p>
   * Specifically, if the RValue is an LValue, it needs to be loaded.
   * If it is a ValueInMuVar, it is already available as a Mu variable so no further process is needed.
   *
   * @return a Mu name whose value (has type @js.value) is the value of the RValue.
   */
  def prepareRValue(rv: RValue): String = rv match {
    case ValueInMuVar(muName) => muName
    case MuLoadable(muName) => {
      val i = emit("loadLocalVar", s"LOAD <@js.value> ${muName}")
      i
    }
    case JSGlobalVar(muName) => {
      val i = emit("loadGlobalVar", s"LOAD <@js.value> ${muName}")
      i
    }
    case PropAccess(baseRV, indexRV) => {
      val baseMN = prepareRValue(baseRV)
      val indexMN = prepareRValue(indexRV)
      val i = emit("propGet", s"CALL <@js.prop_get.sig> @js.prop_get (${baseMN} ${indexMN})")
      i
    }
  }

  def maybeEmitProfilingPoint(node: Node, nameHint: String)(implicit scope: Scope): Unit = {
    if (allowOsr) {
      val curCounterVal = emit(s"loadCounter", s"LOAD <@i64> ${profCounterMuName}")
      val incCounterVal = emit(s"incCounter", s"ADD <@i64> ${curCounterVal} @I64_1")
      val storeCounterVal = emit(s"storeCounter", s"STORE <@i64> ${profCounterMuName} ${incCounterVal}")
      val isCounterBig = emit(s"isCounterBig", s"SGE <@i64> ${curCounterVal} @OPT_THRESHOLD")

      val bbOpt = newBB(s"${nameHint}Opt")
      val bbCheck = newBB(s"${nameHint}AfterProf")

      emit("brCounter", s"BRANCH2 ${isCounterBig} ${bbOpt.muName} ${bbCheck.muName}")

      val backEdgeInfo = new BackEdgeInfo(blFunc, node, curBB, bbOpt)
      blFunc.backEdgeInfos += backEdgeInfo

      use(bbCheck)
    }
  }

  implicit class StringInContextSupport(msg: String) {
    def inCtx(n: Node): String = {
      val sPos = n.getStart()
      val sLine = source.getLine(sPos);
      val sCol = source.getColumn(sPos);

      val marker = SourceHelper.marker(source, n)
      "%d:%d: %s\n%s".format(sLine, sCol, msg, marker)
    }
  }

  def registerRuntimeInstInfo[T <: Node](muName: String, kind: InfoKind[T]) {
    jsClient.registerRuntimeInstInfo(muName, JSRuntimeInstInfo(blFunc, kind))
  }
}

class BaselineCompileError(msg: String = null, cause: Throwable = null) extends RuntimeException(msg, cause)
class UnimplementedFeatureError(msg: String = null, cause: Throwable = null) extends BaselineCompileError(msg, cause)
