package org.microvm.jsmu.compiler.baseline

import scala.collection.mutable.Map

class Scope(parent: Option[Scope]) {
  val varMap = Map[String, LValue]()
  
  def getWithParent(name: String): Option[LValue] = {
    varMap.get(name).orElse (
      parent match {
        case None => None
        case Some(p) => p.getWithParent(name)
      }
    )
  }
}

abstract class RValue
abstract class LValue extends RValue

//case class Magic(name: String) extends RValue
case class ValueInMuVar(muName: String) extends RValue // Computed result. The Mu variable has type @js.value
case class MuLoadable(muName: String) extends LValue   // Local variable. The Mu variable has type iref<@js.value>. Can be loaded and stored.
case class JSGlobalVar(muName: String) extends LValue  // Global variable. The Mu variable has type iref<@js.value>. Can be loaded and stored.
case class PropAccess(obj: RValue, prop: RValue) extends LValue // A property access expression (a[b]).
