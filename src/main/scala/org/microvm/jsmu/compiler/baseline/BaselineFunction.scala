package org.microvm.jsmu.compiler.baseline

import scala.collection.JavaConversions._
import scala.collection.mutable.ArrayBuffer

import org.microvm.jsmu.client.JSFunction
import org.microvm.jsmu.muir._

import jdk.nashorn.internal.ir._

class BackEdgeInfo(val blFunc: BaselineFunction, val node: Node, val headBB: MuBB, val trapBB: MuBB) {
  // patched later because other instructions need to be generated first.
  var trapMuName: String = null
}

class BaselineFunction(val jsFunc: JSFunction, val muFunc: MuFunc) {
  def localVars = jsFunc.localVars
  val backEdgeInfos = new ArrayBuffer[BackEdgeInfo]()

}

class BaselineUnit(val script: String) {
  val funcs = new ArrayBuffer[BaselineFunction]()
  val muBundle = new MuBundle()
  var topLevelFunc: BaselineFunction = null
}
