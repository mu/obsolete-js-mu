package org.microvm.jsmu.compiler

/**
 * Describes the type a JS variable holds.
 */
trait VarType {
}

case object AnyJSVal extends VarType
case object JSDouble extends VarType
case object JSString extends VarType
