package org.microvm.jsmu.compiler
 
object JSTypes extends Enumeration {
  type JSTypes = Value
  val UNDEF, NULL, BOOLEAN, DOUBLE, STRING, CLOSURE, ARRAY = Value
}
