package org.microvm.jsmu.compiler.opt

import scala.collection.mutable.{ HashMap, MultiMap, Set }
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashSet
import scala.collection.mutable.LinkedHashSet
import scala.collection.mutable.MultiMap
import scala.collection.mutable.Stack
import org.microvm.jsmu.compiler.JSTypes
import org.slf4j.LoggerFactory
import com.typesafe.scalalogging.Logger
import scala.annotation.tailrec

trait OptimizingPass {
  def apply(optFunc: OptimizedFunction): Unit = apply(optFunc.hCfg)
  def apply(hCfg: HCFG): Unit = {
    throw new OptimizingError("apply(HCFG) not defined")
  }
}

object CommonOptUtils {
  def getSuccessors(bb: HBB): Seq[HBB] = {
    bb.lastInst match {
      case hi: HInstBr  => Seq(hi.bb)
      case hi: HInstBr2 => Seq(hi.ifTrue, hi.ifFalse)
      case _            => Seq()
    }
  }
}

/**
 * Remove instructions after its first HLastInst and assign it to the lastInst field.
 */
object BasicBlockTrim extends OptimizingPass {
  override def apply(hCfg: HCFG): Unit = {
    for (bb <- hCfg.bbs) {
      bb.insts.zipWithIndex.find({ case (inst, i) => inst.isInstanceOf[HLastInst] }) match {
        case None => throw new OptimizingError("Basic block %s does not have a HLastInst.".format(bb))
        case Some((inst, i)) => {
          bb.lastInst = inst.asInstanceOf[HLastInst]
          bb.insts.remove(i + 1, bb.insts.size - i - 1)
        }
      }
    }
  }
}

/**
 * Remove unreachable basic blocks.
 */
object RemoveUnreachableBasicBlocks extends OptimizingPass {
  override def apply(hCfg: HCFG): Unit = {
    val q = Stack[HBB](hCfg.entry)
    val visited = LinkedHashSet[HBB](hCfg.entry)

    while (!q.isEmpty) {
      val cur = q.pop()
      for (succ <- CommonOptUtils.getSuccessors(cur) if !visited.contains(succ)) {
        visited += succ
        q.push(succ)
      }
    }

    hCfg.bbs.clear()
    hCfg.bbs ++= visited
  }
}

/**
 * Convert the CFG into the SSA form. Also assign the idom and children fields of basic blocks.
 */
object ToSSA extends OptimizingPass {
  val logger: Logger = Logger(LoggerFactory.getLogger(getClass.getName))

  def toSucc(hCfg: HCFG): Seq[Seq[Int]] = {
    val bbToIndex = HashMap[HBB, Int](hCfg.bbs.zipWithIndex: _*)

    ArrayBuffer(hCfg.bbs.map { bb =>
      bb.lastInst match {
        case hi: HInstBr  => Seq(bbToIndex(hi.bb))
        case hi: HInstBr2 => Seq(bbToIndex(hi.ifTrue), bbToIndex(hi.ifFalse))
        case _            => Seq()
      }
    }: _*)
  }

  def entryIndex(hCfg: HCFG): Int = {
    hCfg.bbs.indexOf(hCfg.entry)
  }

  def maskSearch(succ: Seq[Seq[Int]], avoid: Int, entry: Int): Seq[Int] = {
    val reachable = ArrayBuffer.fill(succ.length)(false)

    val q = ArrayBuffer(entry)
    reachable(entry) = true

    while (!q.isEmpty) {
      val curNode = q.remove(q.length - 1)

      for (nextNode <- succ(curNode)) {
        if (nextNode != avoid && reachable(nextNode) == false) {
          reachable(nextNode) = true
          q += nextNode
        }
      }
    }

    for ((r, i) <- reachable.zipWithIndex if i != avoid && r == false) yield i
  }

  def getIdoms(succ: Seq[Seq[Int]], entry: Int): Seq[Int] = {
    val dom = ArrayBuffer.fill(succ.length)(entry)

    for (i <- 0 until succ.length if i != entry) {
      val js = maskSearch(succ, i, entry)
      for (j <- js) {
        if (dom(j) == dom(i)) {
          dom(j) = i
        }
      }
    }

    dom
  }

  def traverseTreeBottomUp(parent: Seq[Int], entry: Int)(f: Int => Unit): Unit = {
    val inCount = ArrayBuffer.fill(parent.length)(0)
    for (n <- parent) {
      inCount(n) += 1
    }

    val q = ArrayBuffer((for ((ic, i) <- inCount.zipWithIndex if ic == 0) yield i): _*)

    while (!q.isEmpty) {
      val x = q.remove(q.length - 1)

      f(x)

      val px = parent(x)
      inCount(px) -= 1
      if (inCount(px) == 0) {
        q += px
      }
    }
  }

  def getDominanceFrontier(succ: Seq[Seq[Int]], idom: Seq[Int], entry: Int): Seq[Seq[Int]] = {
    val df = ArrayBuffer.fill(idom.length)(new HashSet[Int]())

    traverseTreeBottomUp(idom, entry) { x =>
      if (x != entry) {
        // Local dominance
        for (y <- succ(x) if idom(y) != x) df(x) += y

        // Up dominance
        for (y <- df(x) if idom(y) != idom(x)) df(idom(x)) += y
      }
    }

    ArrayBuffer((for (f <- df) yield f.toSeq.sorted): _*)
  }

  def getPhiPlacement(df: Seq[Seq[Int]], init: Seq[Int]): Seq[Int] = {
    val hasAlready = ArrayBuffer.fill(df.length)(false)
    val work = ArrayBuffer.fill(df.length)(false)
    for (x <- init) work(x) = true

    val q = ArrayBuffer(init: _*)

    while (!q.isEmpty) {
      val x = q.remove(q.length - 1)
      for (y <- df(x) if hasAlready(y) == false) {
        hasAlready(y) = true
        if (work(y) == false) {
          work(y) = true
          q += y
        }
      }
    }

    for ((h, i) <- hasAlready.zipWithIndex if h == true) yield i
  }

  /** Transpose a list of parent nodes. */
  def getChildren(parent: Seq[Int], entry: Int): Seq[Seq[Int]] = {
    val children = ArrayBuffer.fill(parent.length)(new ArrayBuffer[Int]())
    for ((p, c) <- parent.zipWithIndex if c != entry) {
      children(p) += c
    }
    children
  }

  override def apply(hCfg: HCFG): Unit = {
    // In the beginning, unreachable basic blocks should be removed.
    // All local variables (those which still uses allocas) are assigned in the entry block.
    val succ = toSucc(hCfg) // successors (in index notation)
    val ei = entryIndex(hCfg) // index of the entry basic block

    val idom = getIdoms(succ, ei) // immediate dominators

    logger.debug("IDOM: " + idom.mkString(", "))

    // Assign idom and children fields
    for ((p, c) <- idom.zipWithIndex if c != ei) {
      val pb = hCfg.bbs(p)
      val cb = hCfg.bbs(c)
      cb.idom = Some(pb)
      pb.children += cb
    }
    hCfg.entry.idom = None

    val df = getDominanceFrontier(succ, idom, ei) // dominance frontier for each basic block

    logger.debug("DF: " + df.map(l => s"[${l.mkString(", ")}]").mkString(", "))

    // Find all occurrences of all HInstLocalPut instructions.
    val assignedIn = HashMap((for (alloca <- hCfg.allocas) yield alloca -> new HashSet[Int]()): _*)

    for ((bb, i) <- hCfg.bbs.zipWithIndex; inst <- bb.insts) inst match {
      case hi: HInstLocalPut => assignedIn(hi.jsName) += i
      case _                 =>
    }

    logger.debug("assigned-in:\n" + (for ((jsName, asgns) <- assignedIn) yield s"${jsName}: ${asgns.mkString(", ")}\n").mkString(""))

    // For each basic block, find the JS vars that needs phi nodes.
    val bbNewPhis = ArrayBuffer.fill(hCfg.bbs.size)(new ArrayBuffer[String]())
    for ((jsName, ai) <- assignedIn) {
      val phiPlaces = getPhiPlacement(df, ai.toSeq)
      for (bbi <- phiPlaces) {
        bbNewPhis(bbi) += jsName
      }
    }

    logger.debug {
      val sb = new StringBuilder("Phi placement:\n")
      for ((vars, bbnum) <- bbNewPhis.zipWithIndex) {
        sb ++= bbnum.toString ++= ": " ++= vars.mkString(", ") ++ "\n"
      }
      sb.toString
    }

    // Insert phi nodes
    val phiToJSName = new HashMap[HPhi, String]()
    for ((vars, bbnum) <- bbNewPhis.zipWithIndex) {
      val bb = hCfg.bbs(bbnum)

      for (jsName <- vars) {
        val phi = new HPhi()
        phi.name = bb.name + "_" + jsName
        phiToJSName(phi) = jsName
        phi.jsName = Some(jsName)
        bb.phis += phi
      }
    }

    // Load/store replacing

    // Forward every HLocalGet to its current SSA variable

    val forward = new HashMap[HInstLocalGet, HValue]()
    val children = getChildren(idom, ei)

    val varStack = HashMap((for (alloca <- hCfg.allocas) yield alloca -> new ArrayBuffer[HValue]()): _*)

    def doBB(bbnum: Int) {
      val bb = hCfg.bbs(bbnum)
      val bps = HashMap(varStack.mapValues(_.length).toSeq: _*)

      // Update current stack of values
      // from phis.
      bb.phis foreach { phi =>
        phi.jsName match {
          case Some(jsName) => varStack(jsName) += phi
          case None         =>
        }
      }

      // From instructions.
      bb.insts foreach {
        case inst: HInstLocalPut => {
          varStack(inst.jsName) += inst.value
        }
        case inst: HInstLocalGet => {
          forward(inst) = varStack(inst.jsName).last
        }
        case _ =>
      }

      // Assign the sources of phi nodes for direct control flow successors.
      for (bbnum2 <- succ(bbnum)) {
        val bb2 = hCfg.bbs(bbnum2)
        for (phi2 <- bb2.phis) {
          phi2.jsName match {
            case Some(jsName) => phi2.bbMap(bb) = varStack(jsName).last
            case _            =>
          }
        }
      }

      // Recursively work on the basic blocks which bb dominates.
      for (childBBNum <- children(bbnum)) {
        doBB(childBBNum)
      }

      // Restore value stack heights.
      for (jsName <- hCfg.allocas) {
        val vs = varStack(jsName)
        val oldHeight = bps(jsName)
        vs.remove(oldHeight, vs.length - oldHeight)
      }
    }

    doBB(ei)

    logger.debug {
      val sb = new StringBuilder("Forward:\n")
      for ((g, v) <- forward) {
        sb ++= g.name ++= " -> " ++= v.name ++ "\n"
      }
      sb.toString
    }

    // Remove HInstLocalGet and HInstLocalPut instructions
    for (bb <- hCfg.bbs) {
      val keptInsts = bb.insts.filter {
        case inst: HInstLocalGet => false
        case inst: HInstLocalPut => false
        case _                   => true
      }

      bb.insts.clear
      bb.insts ++= keptInsts

      for (hvalue <- bb.phis ++ bb.insts) {
        HValue.mapUses(hvalue) {
          case hv: HInstLocalGet => {
            @tailrec
            def recForward(v: HValue): HValue = v match {
              case hi: HInstLocalGet => recForward(forward(hi))
              case hi                => hi
            }
            recForward(hv)
          }
          case hv => hv
        }
      }
    }

    // No longer uses alloca.
    hCfg.allocas.clear()
  }
}

object InferHValueTypes extends OptimizingPass {
  val logger: Logger = Logger(LoggerFactory.getLogger(getClass.getName))

  import CommonOptUtils._
  override def apply(optFunc: OptimizedFunction): Unit = {
    val maybeVarInfo = optFunc.maybeOsrInfo.map(_.varInfo)

    val hCfg = optFunc.hCfg
    val allLocalVars = optFunc.jsFunc.getAllLocalVars()

    // Compute the def-use edges (SSA edges)
    val defUseMap = new HashMap[HValue, Set[HValue]] with MultiMap[HValue, HValue] {
      val emptyDefault = Set.empty[HValue]
      override def default(key: HValue) = emptyDefault
    }

    for (bb <- hCfg.bbs; useVal <- (bb.phis ++ bb.insts); defVal <- HValue.listUses(useVal)) {
      defUseMap.addBinding(defVal, useVal)
    }

    val workingSet = new HashSet[HValue]()
    val q = new Stack[HValue]()

    def addws(hv: HValue): Unit = {
      if (!workingSet.contains(hv)) {
        workingSet.add(hv)
        q.push(hv)
      }
    }

    def updateType(hv: HValue, ht: HValueType): Unit = {
      if (hv.hType isAbove ht) {
        hv.hType = ht
        logger.debug(s"    Set ${hv.name} -> ${ht.toString()}")
        addws(hv)
      }
    }

    // First pass, go through all instructions and fix their types if possible (most are void).
    // Infer the type of parameters since they are given and add to the working set.

    for (bb <- hCfg.bbs; inst <- bb.insts) inst match {
      case hi: HParam => {
        updateType(hi, HPolymorphic)
      }
      case hi: HOSRParam => {
        val paramIndex = hi.index
        val paramJSName = allLocalVars(paramIndex)
        val paramJSType = maybeVarInfo match {
          case Some(varInfo) => varInfo(paramJSName)
          case None => {
            throw new OptimizingError("HOSRParam encountered, but OSRInfo is not available")
          }
        }

        // If it is undefined, we are not sure if it is definitely the value `undefined` or
        // it is just not observed yet. We leave such OSR parameters (may be local variables) as "no information".
        if (paramJSType != JSTypes.UNDEF) {
          updateType(hi, HJSType(paramJSType))
        }
      }
      case hi: HConstLong     => updateType(hi, HInt64)
      case hi: HConstBoolean  => updateType(hi, HJSType(JSTypes.BOOLEAN))
      case hi: HConstDouble   => updateType(hi, HJSType(JSTypes.DOUBLE))
      case hi: HConstString   => updateType(hi, HJSType(JSTypes.STRING))
      case hi: HConstUndef    => updateType(hi, HJSType(JSTypes.UNDEF))
      case hi: HConstNull     => updateType(hi, HJSType(JSTypes.NULL))
      case hi: HConstClosure  => updateType(hi, HJSType(JSTypes.CLOSURE))
      case hi: HInstRet       => hi.hType = HVoid
      case hi: HInstBr        => hi.hType = HVoid
      case hi: HInstBr2       => hi.hType = HVoid
      case hi: HInstGlobalPut => hi.hType = HVoid
      case hi: HInstPropPut   => hi.hType = HVoid
      case hi: HInstBail      => hi.hType = HVoid
      case _                  => {}
    }

    // Propagate the type information until the working set is emtpy.
    while (!q.isEmpty) {
      val cur = q.pop()
      workingSet -= cur

      logger.debug(s"Def ${cur.name}: ${cur.hType}")

      for (use <- defUseMap(cur)) {
        logger.debug(s"  Use ${use.name}: ${use.hType}")

        def handlePoles(defTypes: HValueType*)(handleOtherCases: => Unit): Unit = {
          if (defTypes.contains(HNoInfo)) {
            // do nothing
          } else if (defTypes.contains(HPolymorphic)) {
            updateType(use, HPolymorphic)
          } else {
            handleOtherCases
          }
        }

        use match {
          case hPhi: HPhi => {
            val ct = cur.hType
            val ut = use.hType

            if (ct.level == 1 && ut.level == 1 && ct != ut) {
              updateType(use, HPolymorphic)
            } else {
              updateType(use, ct)
            }
          }
          case hi: HInstBinOp => {
            val (lt, rt) = (hi.lhs.hType, hi.rhs.hType)
            handlePoles(lt, rt) {
              hi.op match {
                case HBinOp.ADD => (lt, rt) match {
                  case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => updateType(use, HJSType(JSTypes.DOUBLE)) // num + num -> num
                  case (HJSType(JSTypes.STRING), HJSType(JSTypes.STRING)) => updateType(use, HJSType(JSTypes.STRING)) // str + str -> str
                  case _ => {}
                }
                case HBinOp.SUB | HBinOp.MUL | HBinOp.DIV => (lt, rt) match {
                  case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => updateType(use, HJSType(JSTypes.DOUBLE)) // num [-*/] num -> num
                  case _ => {}
                }
                case HBinOp.EQ | HBinOp.LT => (lt, rt) match {
                  case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => updateType(use, HJSType(JSTypes.BOOLEAN)) // num [=<] num -> boolean
                  case (HJSType(JSTypes.STRING), HJSType(JSTypes.STRING)) => updateType(use, HJSType(JSTypes.BOOLEAN)) // str [=<] str -> boolean
                  case _ => {}
                }
              }
            }
          }
          case hi: HInstUnOp => {
            val ot = hi.opnd.hType
            handlePoles(ot) {
              hi.op match {
                case HUnOp.NOT => ot match {
                  case HJSType(JSTypes.BOOLEAN) => updateType(use, HJSType(JSTypes.BOOLEAN)) // !boolean -> boolean
                  case _                        => {}
                }
                case HUnOp.NEG => ot match {
                  case HJSType(JSTypes.DOUBLE) => updateType(use, HJSType(JSTypes.DOUBLE)) // -num -> num
                  case _                       => {}
                }
              }
            }
          }
          case _ => {
            // Oops. The current type inferer can only infer types from binops and unops :(.... 
          }
        }
      }
    }

    // Infer HValue representations

    // Traverse all basic blocks, visit dominators before children to ensure defs are visited before uses (except phis)
    val q2 = Stack[HBB](hCfg.entry)
    while (!q2.isEmpty) {
      val cur = q2.pop()

      for (hv <- cur.phis ++ cur.insts) {
        val hvt = hv.hType
        hv.repr = hvt match {
          case HNoInfo      => HValueRepr.AS_TAGREF
          case HPolymorphic => HValueRepr.AS_TAGREF
          case HVoid        => HValueRepr.NO_REPR
          case HInt64       => HValueRepr.AS_I64
          case HJSType(jsType) => jsType match {
            case JSTypes.UNDEF   => HValueRepr.AS_TAGREF
            case JSTypes.NULL    => HValueRepr.AS_TAGREF
            case JSTypes.DOUBLE  => HValueRepr.AS_DOUBLE
            case JSTypes.STRING  => HValueRepr.AS_TAGREF
            case JSTypes.CLOSURE => HValueRepr.AS_TAGREF
            case JSTypes.ARRAY   => HValueRepr.AS_TAGREF
            case JSTypes.BOOLEAN => {
              val uses = defUseMap(hv)

              // If the only use is HInstBr2, then it can be represented as an int<1>
              if (uses.size == 1) {
                val theUse = uses.head
                if (theUse.isInstanceOf[HInstBr2]) {
                  // As a side effect, if it has the form !(a<b) or !(a==b) as generated from > or >=,
                  // we can combine them into one instruction and remove the former.
                  if (hv.isInstanceOf[HInstUnOp]) {
                    hv.asInstanceOf[HInstUnOp].opnd.repr = HValueRepr.OMITTED
                  }
                  HValueRepr.AS_I1
                } else {
                  HValueRepr.AS_TAGREF
                }
              } else {
                HValueRepr.AS_TAGREF
              }
            }
          }
        }
      }

      for (child <- cur.children) {
        q2.push(child)
      }
    }
  }
}

object AddBoxing extends OptimizingPass {
  def makeHInstBoxFor(hv: HValue): HInstBox = {
    val hib = new HInstBox(hv)
    hib.name = "box_" + hv.name
    hib.hType = hv.hType
    hib.repr = HValueRepr.AS_TAGREF
    hib
  }

  override def apply(hCfg: HCFG): Unit = {
    val addBefore = new HashMap[HInst, Set[HInst]] with MultiMap[HInst, HInst]

    // Create appropriate HInstBox instructions and map from appropriate instructions.
    for (bb <- hCfg.bbs) {
      for (phi <- bb.phis) {
        if (phi.repr == HValueRepr.AS_TAGREF) {
          for (bb <- phi.bbMap.keySet) {
            val hv = phi.bbMap(bb)
            if (hv.repr != HValueRepr.AS_TAGREF) {
              val hib = makeHInstBoxFor(hv)
              addBefore.addBinding(bb.lastInst, hib)
              phi.bbMap(bb) = hib
            }
          }
        }
      }
    }

    // Update each basic block, inserting HInstBox appropriately.
    for (bb <- hCfg.bbs) {
      val newInstList = new ArrayBuffer[HInst]()
      for (inst <- bb.insts) {
        if (addBefore.contains(inst)) {
          newInstList ++= addBefore(inst)
        }
        newInstList += inst
      }
      bb.insts.clear()
      bb.insts ++= newInstList
    }
  }
}

class OptimizingError(msg: String = null, cause: Throwable = null) extends OptimizingCompilerError(msg, cause)
