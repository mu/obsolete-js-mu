package org.microvm.jsmu.compiler.opt

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap
import org.microvm.jsmu.client._
import org.microvm.jsmu.compiler.AbstractMuEmitter
import org.microvm.jsmu.muir._
import org.microvm.jsmu.muir.ImplicitConversions._
import org.microvm.jsmu.compiler.opt.HValueRepr.HValueRepr
import org.microvm.jsmu.compiler.JSTypes
import org.microvm.jsmu.compiler.JSTypes.JSTypes

class HIRToMuIR(val optimizingCompiler: OptimizingCompiler,
                val optFunc: OptimizedFunction,
                val eager: Boolean = false) extends {
  val jsFunc = optFunc.jsFunc
  val funcMuName = optimizingCompiler.optFuncNamer.next(jsFunc.nameHint)
  val paramListMuName = funcMuName % "params"
  val jsClient = optimizingCompiler.jsClient // used by AbstractMuEmitter
  val isOSR = optFunc.maybeOsrInfo.isDefined

  private val muSig = if (isOSR) {
    "@js.userfunc_with_osr.sig"
  } else {
    "@js.userfunc.sig"
  }
} with MuIRFunctionBuilder(funcMuName, muSig, Seq(paramListMuName))
    with AbstractMuEmitter {
  val hCfg = optFunc.hCfg

  val hbbToMubb = new HashMap[HBB, MuBB]()
  val hvalueToMuname = new HashMap[HValue, String]()

  private def mn(hbb: HBB): String = {
    hbbToMubb(hbb).muName
  }

  private def mn(hv: HValue): String = try {
    hvalueToMuname(hv)
  } catch {
    case e: NoSuchElementException => {
      throw new OptimizingCompilerError("Mu name of HValue %s not found.".format(hv.name), e)
    }
  }

  /**
   * One HBB does not scrictly correspond to one MuBB, especially when function call is involved.
   * This map maps each HBB to the MuBB its branching instruction is in.
   */
  val lastInstMubb = new HashMap[HBB, MuBB]()

  /** Map each local variable (still eligible for alloca) to the Mu name of its ALLOCA. */
  val jsNameToAllocas = new HashMap[String, String]()

  private def allocaOf(jsName: String): String = {
    jsNameToAllocas(jsName)
  }

  /** The Mu name of the global cell for js global variable jsName */
  private def globalOf(jsName: String): String = {
    jsClient.globalPool.getGlobalCell(jsName)
  }

  /**
   * A list of mu names. If not doing OSR, each represents an extracted parameter (as @js.value).
   * If doing OSR, each represents an extracted OSR parameter, i.e. the value of all JS parameters and JS local
   * variables.
   */
  val paramMuNames = new ArrayBuffer[String]()

  build()

  def build() {
    makeBasicBlocks()

    makeAllocas()

    makeParams()

    makeInsts()
  }

  def makeBasicBlocks() {
    hbbToMubb(hCfg.entry) = entry

    for (hbb <- hCfg.bbs if hbb != hCfg.entry) {
      val muBB = newBB(hbb.name)
      hbbToMubb(hbb) = muBB
    }
  }

  def makeAllocas() {
    for (jsName <- hCfg.allocas) {
      val alloca = emit(s"alloca_${jsName}", "ALLOCA <@js.value>")
      jsNameToAllocas(jsName) = alloca
    }
  }

  def makeParams() {
    if (isOSR) {
      // Working with the "osrentry" basic block.
      use(entry)

      val plIRMuName = emit(s"paramsIR", s"GETIREF <@js.paramList> ${paramListMuName}")
      val plVarMuName = emit(s"paramsVar", s"GETVARPARTIREF <@js.paramList> ${plIRMuName}")

      // During OSR, exactly as many params as the number of JS params + local variable are provided. No bounds check
      // needed.

      var curVarSlotMuName = plVarMuName
      jsFunc.getAllLocalVars().zipWithIndex.foreach {
        case (param, i) =>
          val paramValMuName = emit(s"osrParam_${param}_${i}", s"LOAD <@js.value> ${curVarSlotMuName}")
          paramMuNames += paramValMuName
          curVarSlotMuName = emit(s"shiftIRTo${i + 1}", s"SHIFTIREF <@js.value @i64> ${curVarSlotMuName} @I64_1")
      }
    } else {
      // Not doing OSR. Extract params like usual.
      use(entry)

      // Just extract parameters. Unlike baseline, a parameter may not have an ALLOCA cell. 
      val plIRMuName = emit(s"paramsIR", s"GETIREF <@js.paramList> ${paramListMuName}")
      val plFixMuName = emit(s"paramsFix", s"GETFIXEDPARTIREF <@js.paramList> ${plIRMuName}")
      val plLenMuName = emit(s"paramsLen", s"LOAD <@i64> ${plFixMuName}")
      var indexMuName = "@I64_0"
      hCfg.params.zipWithIndex.foreach {
        case (param, i) =>
          val paramValMuName = emit(s"getParam${i}", s"CALL <@js.get_param.sig> @js.get_param (${paramListMuName} ${plLenMuName} ${indexMuName})")
          paramMuNames += paramValMuName
          indexMuName = emit(s"incIndexTo${i + 1}", s"ADD <@i64> ${indexMuName} @I64_1")
      }
    }
  }

  def makeInsts() {

    for (hbb <- hCfg.bbs) {
      for (hphi <- hbb.phis) {
        val phiMuName = instNamer.next(hphi.name)
        hvalueToMuname(hphi) = phiMuName
      }
    }

    for (hbb <- hCfg.bbs) {
      val muBB = hbbToMubb(hbb)
      use(muBB)

      for (hinst <- hbb.insts) {
        makeInst(hbb, hinst)
      }
    }

    for (hbb <- hCfg.bbs) {
      val muBB = hbbToMubb(hbb)
      use(muBB)

      val tempBuffer = new ArrayBuffer[MuInst]()
      for (hphi <- hbb.phis) {
        makePhi(hphi, tempBuffer)
      }
      muBB.insts.prependAll(tempBuffer)
    }
  }

  def makePhi(hphi: HPhi, tempBuffer: ArrayBuffer[MuInst]) {
    val sb = new StringBuilder()
    for ((hbb, hv) <- hphi.bbMap) {
      val muBB = lastInstMubb(hbb).muName
      val muVar = mn(hv)

      sb ++= muBB ++= ": " ++= muVar ++= "; "
    }

    val muTy = reprToMuTy(hphi.repr)

    tempBuffer += new MuInst(mn(hphi), s"PHI <${muTy}> { ${sb.toString()} }")
  }

  def makeInst(hbb: HBB, hinst: HInst): Unit = try {
    val thisMuName: String = hinst match {
      // HParam and HOSRParam do not generate Mu instructions, but when referenced, the pre-generated Mu instructions
      // that loads the parameters are referenced.
      case hi: HParam => paramMuNames(hi.index)
      case hi: HOSRParam => {
        val pmn = paramMuNames(hi.index)
        if (hi.repr != HValueRepr.AS_TAGREF) {
          makeUnbox(pmn, hi.repr)
        } else {
          pmn
        }
      }
      case hi: HConstLong => constLong(hi.value)
      case hi: HConstBoolean => hi.repr match {
        case HValueRepr.AS_I1 => {
          if (hi.value == true) { "@I1_1" } else { "@I1_0" }
        }
        case HValueRepr.AS_TAGREF => {
          if (hi.value == true) {
            emit(hi.name, s"COMMINST @uvm.tr64.from_ref (@NULL @js.true.tag)")
          } else {
            emit(hi.name, s"COMMINST @uvm.tr64.from_ref (@NULL @js.false.tag)")
          }
        }
      }

      case hi: HConstDouble => hi.repr match {
        case HValueRepr.AS_DOUBLE =>
          constDouble(hi.value)
        case HValueRepr.AS_TAGREF =>
          emit(hi.name, s"COMMINST @uvm.tr64.from_fp (${constDouble(hi.value)})")
      }
      case hi: HConstString => hi.repr match {
        case HValueRepr.AS_TAGREF =>
          emit(hi.name, s"LOAD <@js.value> ${constString(hi.value)}")
      }
      case hi: HConstUndef => hi.repr match {
        case HValueRepr.AS_TAGREF => emit(hi.name, s"COMMINST @uvm.tr64.from_ref (@NULL @js.undef.tag)")
      }
      case hi: HConstNull => hi.repr match {
        case HValueRepr.AS_TAGREF => emit(hi.name, s"COMMINST @uvm.tr64.from_ref (@NULL @js.null.tag)")
      }
      case hi: HConstClosure => hi.repr match {
        case HValueRepr.AS_TAGREF =>
          if (eager) {
            emit(hi.name, s"LOAD <@js.value> ${constOptimizedClosure(hi.jsFunc)}")
          } else {
            emit(hi.name, s"LOAD <@js.value> ${constBaselineClosure(hi.jsFunc)}")
          }
      }

      case hi: HInstRet => {
        lastInstMubb(hbb) = curBB
        val rv = ensureBoxed(hi.value)
        emit(hi.name, s"RET <@js.value> ${rv}")
      }
      case hi: HInstBr => {
        lastInstMubb(hbb) = curBB
        emit(hi.name, s"BRANCH ${mn(hi.bb)}")
      }
      case hi: HInstBr2 => {
        lastInstMubb(hbb) = curBB
        val truthI1 = hi.cond.repr match {
          case HValueRepr.AS_I1 => mn(hi.cond)
          case HValueRepr.AS_TAGREF => {
            val truthMuName = emit("truthTest", s"CALL <@js.truth.sig> @js.truth (${mn(hi.cond)})")
            emit("truthI1", s"EQ <@i64> ${truthMuName} @I64_1")
          }
        }
        emit(hi.name, s"BRANCH2 ${truthI1} ${mn(hi.ifTrue)} ${mn(hi.ifFalse)}")
      }

      case hi: HInstBinOp => {
        val lhsMN = mn(hi.lhs)
        val rhsMN = mn(hi.rhs)

        hi.op match {
          case HBinOp.ADD => {
            (hi.lhs.hType, hi.rhs.hType) match {
              case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => {
                emitArithDoubleBinOp(hi, "FADD", lhsMN, rhsMN)
              }
              case (HJSType(JSTypes.STRING), HJSType(JSTypes.STRING)) => {
                val lhsUnbox = makeUnbox(lhsMN, HValueRepr.AS_REFSTRING)
                val rhsUnbox = makeUnbox(rhsMN, HValueRepr.AS_REFSTRING)
                val rvRs = emit(hi.name, s"CALL <@js.concat_str.sig> @js.concat_str (${lhsUnbox} ${rhsUnbox})")
                makeBox(rvRs, HValueRepr.AS_REFSTRING)
              }
              case _ => {
                emitGenBinOp(hi, "@js.add.gen", hi.lhs, hi.rhs)
              }
            }
          }
          case HBinOp.SUB => {
            (hi.lhs.hType, hi.rhs.hType) match {
              case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => {
                emitArithDoubleBinOp(hi, "FSUB", lhsMN, rhsMN)
              }
              case _ => {
                emitGenBinOp(hi, "@js.sub.gen", hi.lhs, hi.rhs)
              }
            }
          }
          case HBinOp.MUL => {
            (hi.lhs.hType, hi.rhs.hType) match {
              case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => {
                emitArithDoubleBinOp(hi, "FMUL", lhsMN, rhsMN)
              }
              case _ => {
                emitGenBinOp(hi, "@js.mul.gen", hi.lhs, hi.rhs)
              }
            }
          }
          case HBinOp.DIV => {
            (hi.lhs.hType, hi.rhs.hType) match {
              case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => {
                emitArithDoubleBinOp(hi, "FDIV", lhsMN, rhsMN)
              }
              case _ => {
                emitGenBinOp(hi, "@js.div.gen", hi.lhs, hi.rhs)
              }
            }
          }
          case HBinOp.EQ => {
            if (hi.repr == HValueRepr.OMITTED) throw new SkipHInst

            (hi.lhs.hType, hi.rhs.hType) match {
              case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => {
                emitRelDoubleBinOp(hi, "FOEQ", lhsMN, rhsMN, reprFor=hi)
              }
              case (HJSType(JSTypes.STRING), HJSType(JSTypes.STRING)) => {
                emitRelStringBinOp(hi, lhsMN, rhsMN, "@I64_0")
              }
              case _ => {
                emitGenBinOp(hi, "@js.eqs.gen", hi.lhs, hi.rhs)
              }
            }
          }
          case HBinOp.LT => {
            if (hi.repr == HValueRepr.OMITTED) throw new SkipHInst

            (hi.lhs.hType, hi.rhs.hType) match {
              case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => {
                emitRelDoubleBinOp(hi, "FOLT", lhsMN, rhsMN, reprFor=hi)
              }
              case (HJSType(JSTypes.STRING), HJSType(JSTypes.STRING)) => {
                emitRelStringBinOp(hi, lhsMN, rhsMN, "@I64_N1")
              }
              case _ => {
                emitGenBinOp(hi, "@js.lt.gen", hi.lhs, hi.rhs)
              }
            }
          }
        }
      }

      case hi: HInstUnOp => {
        def emitGenUnOp(muFunc: String, opnd: HValue): String = {
          val opndTRMN = ensureBoxed(opnd)
          emit(hi.name, s"CALL <@js.unop.gen.sig> ${muFunc} (${opnd})")
        }

        hi.op match {
          case HUnOp.NOT => {
            hi.opnd.repr match {
              case HValueRepr.OMITTED => {
                hi.opnd match {
                  case hi2: HInstBinOp => {
                    hi2.op match {
                      case HBinOp.EQ => {
                        (hi2.lhs.hType, hi2.rhs.hType) match {
                          case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => {
                            emitRelDoubleBinOp(hi2, "FONE", mn(hi2.lhs), mn(hi2.rhs), reprFor=hi)
                          }
                          case (HJSType(JSTypes.STRING), HJSType(JSTypes.STRING)) => {
                            emitRelStringBinOp(hi2, mn(hi2.lhs), mn(hi2.rhs), "@I64_0", neg = true)
                          }
                          case _ => {
                            val rel = emitGenBinOp(hi2, "@js.eqs.gen", hi2.lhs, hi2.rhs)
                            emit(hi.name, s"CALL <@js.unop.gen.sig> @js.not.gen (${rel})")
                          }
                        }
                      }
                      case HBinOp.LT => {
                        (hi2.lhs.hType, hi2.rhs.hType) match {
                          case (HJSType(JSTypes.DOUBLE), HJSType(JSTypes.DOUBLE)) => {
                            emitRelDoubleBinOp(hi2, "FOGE", mn(hi2.lhs), mn(hi2.rhs), reprFor=hi)
                          }
                          case (HJSType(JSTypes.STRING), HJSType(JSTypes.STRING)) => {
                            emitRelStringBinOp(hi2, mn(hi2.lhs), mn(hi2.rhs), "@I64_N1", neg = true)
                          }
                          case _ => {
                            val rel = emitGenBinOp(hi2, "@js.lt.gen", hi2.lhs, hi2.rhs)
                            emit(hi.name, s"CALL <@js.unop.gen.sig> @js.not.gen (${rel})")
                          }
                        }
                      }
                    }
                  }
                } // All other cases are unexpected.
              }
              case HValueRepr.AS_TAGREF => {
                emitGenUnOp("@js.not.gen", hi.opnd)
              }
            }
          }
          case HUnOp.NEG => {
            hi.opnd.repr match {
              case HValueRepr.AS_DOUBLE => {
                val compVal = emit(hi.name, s"FSUB <@double> @D_0 ${mn(hi.opnd)}")
                hi.repr match {
                  case HValueRepr.AS_DOUBLE => compVal
                  case HValueRepr.AS_TAGREF => makeBox(compVal, HValueRepr.AS_DOUBLE)
                }
              }
              case HValueRepr.AS_TAGREF => {
                emitGenUnOp("@js.neg.gen", hi.opnd)
              }
            }
          }
        }
      }

      case hi: HInstCall      => makeCall(hi)

      case hi: HInstLocalGet  => emit(hi.name, s"LOAD <@js.value> ${allocaOf(hi.jsName)}")
      case hi: HInstLocalPut  => emit(hi.name, s"STORE <@js.value> ${allocaOf(hi.jsName)} ${ensureBoxed(hi.value)}")
      case hi: HInstGlobalGet => emit(hi.name, s"LOAD <@js.value> ${globalOf(hi.jsName)}")
      case hi: HInstGlobalPut => emit(hi.name, s"STORE <@js.value> ${globalOf(hi.jsName)} ${ensureBoxed(hi.value)}")
      case hi: HInstPropGet   => emit(hi.name, s"CALL <@js.prop_get.sig> @js.prop_get (${ensureBoxed(hi.obj)} ${ensureBoxed(hi.prop)})")
      case hi: HInstPropPut   => emit(hi.name, s"CALL <@js.prop_put.sig> @js.prop_put (${ensureBoxed(hi.obj)} ${ensureBoxed(hi.prop)} ${ensureBoxed(hi.value)})")

      case hi: HInstBail => {
        emit(hi.name, s"TRAP <@void>")
      }

      case hi: HInstBox => {
        val srcMn = mn(hi.opnd)
        val srcHr = hi.opnd.repr
        makeBox(srcMn, srcHr)
      }
    }

    hvalueToMuname(hinst) = thisMuName
  } catch {
    case e: SkipHInst => // skip this instruction
  }

  def makeCall(hc: HInstCall): String = {
    val calleeMN = ensureBoxed(hc.callee)

    if (hc.callee.hType != HJSType(JSTypes.CLOSURE)) {
      val bbNotClosure = newBB("notClosure")
      val bbIsRef = newBB("isRef")
      val bbIsClosure = newBB("isClosure")

      val calleeIsRef = emit("calleeIsRef", s"COMMINST @uvm.tr64.is_ref (${calleeMN})")
      emit("brIsRef", s"BRANCH2 ${calleeIsRef} ${bbIsRef.muName} ${bbNotClosure.muName}")

      use(bbIsRef)
      val calleeTag = emit("getTagFromCallee", s"COMMINST @uvm.tr64.to_tag (${calleeMN})")
      val calleeTagIsClosure = emit("tagIsClosure", s"EQ <@i6> ${calleeTag} @js.closure.tag")
      emit("brIsClosure", s"BRANCH2 ${calleeTagIsClosure} ${bbIsClosure.muName} ${bbNotClosure.muName}")

      use(bbNotClosure)
      val errorCall = emit("callError", s"CALL <@js.error.sig> @js.error () KEEPALIVE (${calleeMN})")
      emit("unreachable", s"THROW @NULL")
      use(bbIsClosure)
    }

    val refClosureVoid = emit("toRef", s"COMMINST @uvm.tr64.to_ref (${calleeMN})")
    val refClosure = emit("toRefClosure", s"REFCAST <@refvoid @js.closure.r> ${refClosureVoid}")
    val iRefClosure = emit("toIRefClosure", s"GETIREF <@js.closure> ${refClosure}")
    val iRefFunc = emit("toIRefFunc", s"GETFIELDIREF <@js.closure 0> ${iRefClosure}")
    val funcMN = emit("loadFuncFromClosure", s"LOAD <@js.userfunc> ${iRefFunc}")

    val argsMN = hc.args.map(ensureBoxed)

    val nArgs = hc.args.length
    val nArgsMN = constLong(nArgs)

    val argList = emit("newArgList", s"NEWHYBRID <@js.paramList @i64> ${nArgsMN}")
    val argListIr = emit("argListIr", s"GETIREF <@js.paramList> ${argList}")
    val argListFixIr = emit("argListFixIr", s"GETFIXEDPARTIREF <@js.paramList> ${argListIr}")
    emit(s"putParamLen", s"STORE <@i64> ${argListFixIr} ${nArgsMN}")

    val argListVarIr = emit("argListVarIr", s"GETVARPARTIREF <@js.paramList> ${argListIr}")
    val argIndex = "@I64_0"

    var curIndexVal = argIndex
    for ((arg, i) <- argsMN.zipWithIndex) {
      val argElem = emit(s"argElem${i}", s"SHIFTIREF <@js.value @i64> ${argListVarIr} ${curIndexVal}")
      emit(s"putArg${i}", s"STORE <@js.value> ${argElem} ${arg}")
      curIndexVal = emit(s"incArgTo${i + 1}", s"ADD <@i64> ${curIndexVal} @I64_1")
    }

    val call = emit("call", s"CALL <@js.userfunc.sig> ${funcMN} (${argList})")
    call
  }

  /** Unbox a tagref64 value to the representation of hr. */
  def makeUnbox(srcMn: String, hr: HValueRepr): String = {
    hr match {
      case HValueRepr.AS_DOUBLE => {
        emit(s"unboxToDouble", s"COMMINST @uvm.tr64.to_fp (${srcMn})")
      }
      case HValueRepr.AS_REFSTRING => {
        val rv = emit(s"unboxToString", s"COMMINST @uvm.tr64.to_ref (${srcMn})")
        emit(s"castToRefString", s"REFCAST <@refvoid @js.string.r> ${rv}")
      }
      case _ => {
        throw new OptimizingCompilerError(s"Unbox to ${hr.toString} is not supported")
      }
    }
  }

  /** Box a value into tagref64. */
  def makeBox(srcMn: String, hr: HValueRepr): String = {
    hr match {
      case HValueRepr.AS_DOUBLE => {
        emit(s"boxFromDouble", s"COMMINST @uvm.tr64.from_fp (${srcMn})")
      }
      case HValueRepr.AS_REFSTRING => {
        val rv = emit(s"castToTR", s"REFCAST <@js.string.r @refvoid> ${srcMn}")
        emit(s"boxFromString", s"COMMINST @uvm.tr64.from_ref (${rv} @js.string.tag)")
      }
      case _ => {
        throw new OptimizingCompilerError(s"Boxing from ${hr.toString} is not supported")
      }
    }
  }

  def reprToMuTy(hr: HValueRepr): String = hr match {
    case HValueRepr.AS_DOUBLE => "@double"
    case HValueRepr.AS_TAGREF => "@js.value"
    case HValueRepr.AS_I1     => "@i1"
    case HValueRepr.AS_I64    => "@i64"
    case _ => {
      throw new OptimizingCompilerError(s"Representation ${hr.toString} is not supported")
    }
  }

  def ensureBoxed(hv: HValue): String = hv.repr match {
    case HValueRepr.AS_TAGREF => mn(hv)
    case r                    => makeBox(mn(hv), r)
  }

  def emitGenBinOp(hi: HInst, muFunc: String, lhs: HValue, rhs: HValue): String = {
    val lhsTRMN = ensureBoxed(lhs)
    val rhsTRMN = ensureBoxed(rhs)
    emit(hi.name, s"CALL <@js.binop.gen.sig> ${muFunc} (${lhs} ${rhs})")
  }

  def emitArithDoubleBinOp(hi: HInst, muInst: String, lhs: String, rhs: String): String = {
    val compVal = emit(hi.name, s"${muInst} <@double> ${lhs} ${rhs}")
    hi.repr match {
      case HValueRepr.AS_DOUBLE => compVal
      case HValueRepr.AS_TAGREF => makeBox(compVal, HValueRepr.AS_DOUBLE)
    }
  }

  def emitRelDoubleBinOp(hi: HInst, muInst: String, lhs: String, rhs: String, reprFor: HInst): String = {
    val compVal = emit(hi.name, s"${muInst} <@double> ${lhs} ${rhs}")
    // If hi.repr is omitted, adjust the representation for reprFor, which is a UnOp.
    reprFor.repr match {
      case HValueRepr.AS_I1 => compVal
      case HValueRepr.AS_TAGREF => {
        val tru = emit("true", s"COMMINST @uvm.tr64.from_ref (@NULL @js.true.tag)")
        val fal = emit("false", s"COMMINST @uvm.tr64.from_ref (@NULL @js.false.tag)")
        emit(hi.name, s"SELECT <@i1> ${compVal} ${tru} ${fal}")
      }
    }
  }

  def emitRelStringBinOp(hi: HInst, lhs: String, rhs: String, trueIf: String, neg: Boolean = false): String = {
    val lhsUnbox = makeUnbox(lhs, HValueRepr.AS_REFSTRING)
    val rhsUnbox = makeUnbox(rhs, HValueRepr.AS_REFSTRING)
    val rvI64 = emit(hi.name + "_call", s"CALL <@js.cmp_str.sig> @js.cmp_str (${lhsUnbox} ${rhsUnbox})")
    val inst = if (neg) "NE" else "EQ"
    val compVal = emit(hi.name, s"${inst} <@i64> ${rvI64} ${trueIf}")

    hi.repr match {
      case HValueRepr.AS_I1 => compVal
      case HValueRepr.AS_TAGREF => {
        val tru = emit("true", s"COMMINST @uvm.tr64.from_ref (@NULL @js.true.tag)")
        val fal = emit("false", s"COMMINST @uvm.tr64.from_ref (@NULL @js.false.tag)")
        emit(hi.name, s"SELECT <@i1> ${compVal} ${tru} ${fal}")
      }
    }
  }
}

class SkipHInst extends RuntimeException("Instruction skipped")