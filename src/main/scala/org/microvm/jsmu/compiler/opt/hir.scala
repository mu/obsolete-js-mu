package org.microvm.jsmu.compiler.opt

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap

import org.microvm.jsmu.client.JSFunction

import HValueRepr.HValueRepr

class HCFG(val name: String) {
  val params = new ArrayBuffer[String]() // only param names. The HParam(i) instruction gets the i-th param.
  val localVars = new ArrayBuffer[String]()
  val allocas = new ArrayBuffer[String]() // params or localVars still uses ALLOCA (not eliminated for various reasons, e.g. not running any optimisation yet)
  val bbs = new ArrayBuffer[HBB]()
  var entry: HBB = null
}

class HBB(val name: String) {
  val phis = new ArrayBuffer[HPhi]()
  val insts = new ArrayBuffer[HInst]()
  var lastInst: HLastInst = null
  
  /** The current bb's immediate dominator. None if this is entry. */
  var idom: Option[HBB] = None
  /** A list of bbs this bb dominates. */
  val children = new ArrayBuffer[HBB]()
}

abstract class HValue {
  var name: String = "unnamed"
  
  /** What value this instruction holds/produces. */
  var hType: HValueType = HNoInfo
  /** How the value is represented as Mu type. */
  var repr: HValueRepr = HValueRepr.NO_REPR
}

class HPhi extends HValue {
  val bbMap = new HashMap[HBB, HValue]()
  /** If a phi represents a JS var, jsName is Some(variableName); otherwise None. */
  var jsName: Option[String] = None
}

abstract class HInst extends HValue

class HParam(val index: Int) extends HInst
class HOSRParam(val index: Int) extends HInst

abstract class HConst extends HInst
class HConstLong(val value: Long) extends HConst // Not directly from JS literals. Used internally.
class HConstBoolean(val value: Boolean) extends HConst
class HConstDouble(val value: Double) extends HConst
class HConstString(val value: String) extends HConst
class HConstUndef() extends HConst
class HConstNull() extends HConst
class HConstClosure(val jsFunc: JSFunction) extends HConst

abstract class HLastInst extends HInst

class HInstRet(var value: HValue) extends HLastInst
class HInstBr(val bb: HBB) extends HLastInst
class HInstBr2(var cond: HValue, val ifTrue: HBB, val ifFalse: HBB) extends HLastInst

object HBinOp extends Enumeration {
  type HBinOp = Value
  val ADD, SUB, MUL, DIV, EQ, LT = Value
}

import HBinOp.HBinOp

object HUnOp extends Enumeration {
  type HUnOp = Value
  val NOT, NEG = Value
}

import HUnOp.HUnOp

class HInstBinOp(val op: HBinOp, var lhs: HValue, var rhs: HValue) extends HInst

class HInstUnOp(val op: HUnOp, var opnd: HValue) extends HInst

class HInstCall(var callee: HValue, var args: Seq[HValue]) extends HInst

class HInstLocalGet(val jsName: String) extends HInst
class HInstLocalPut(val jsName: String, var value: HValue) extends HInst
class HInstGlobalGet(val jsName: String) extends HInst
class HInstGlobalPut(val jsName: String, var value: HValue) extends HInst
class HInstPropGet(var obj: HValue, var prop: HValue) extends HInst
class HInstPropPut(var obj: HValue, var prop: HValue, var value: HValue) extends HInst

class HInstArrayNewEmpty(var sz: HValue) extends HInst
class HInstArrayNewLit(var elems: Seq[HValue]) extends HInst

class HInstBail() extends HInst

/**
 * HInstBox is inserted by the AddBoxing optimizing pass. It hints the low-level
 * Mu code generator to convert a value from one representation to the tagged
 * reference representation. On the high level, it is an identity operation.
 */
class HInstBox(val opnd: HValue) extends HInst

object HValue {
  def listUses(hvalue: HValue): Seq[HValue] = hvalue match {
    case hp: HPhi => {
      hp.bbMap.values.toSeq
    }
    case hi: HInstRet           => Seq(hi.value)
    case hi: HInstBr2           => Seq(hi.cond)
    case hi: HInstBinOp         => Seq(hi.lhs, hi.rhs)
    case hi: HInstUnOp          => Seq(hi.opnd)
    case hi: HInstCall          => Seq(hi.callee) ++ hi.args
    case hi: HInstPropGet       => Seq(hi.obj, hi.prop)
    case hi: HInstPropPut       => Seq(hi.obj, hi.prop, hi.value)
    case hi: HInstArrayNewEmpty => Seq(hi.sz)
    case hi: HInstArrayNewLit   => hi.elems
    case _                      => Seq()
  }
  
  def mapUses(hvalue: HValue)(f: HValue => HValue): Unit = hvalue match {
    case hp: HPhi => {
      for (bb <- hp.bbMap.keys) {
        hp.bbMap(bb) = f(hp.bbMap(bb))
      }
    }
    case hi: HInstRet           => { hi.value = f(hi.value) }
    case hi: HInstBr2           => { hi.cond = f(hi.cond) }
    case hi: HInstBinOp         => { hi.lhs = f(hi.lhs); hi.rhs = f(hi.rhs) }
    case hi: HInstUnOp          => { hi.opnd = f(hi.opnd) }
    case hi: HInstCall          => { hi.callee = f(hi.callee); hi.args = hi.args.map(f) }
    case hi: HInstPropGet       => { hi.obj = f(hi.obj); hi.prop = f(hi.prop) }
    case hi: HInstPropPut       => { hi.obj = f(hi.obj); hi.prop = f(hi.prop); hi.value = f(hi.value) }
    case hi: HInstArrayNewEmpty => { hi.sz = f(hi.sz) }
    case hi: HInstArrayNewLit   => { hi.elems = hi.elems.map(f) }
    case _                      =>
  }
}

object HIR {
  def prettyPrint(hcfg: HCFG)(implicit sb: StringBuilder): StringBuilder = {
    sb ++= s"HCFG ${hcfg.name} {\n"
    sb ++= s"  ENTRYBLOCK = ${hcfg.entry.name}\n"
    sb ++= "  PARAMS     = " ++= hcfg.params.mkString(", ") ++= "\n"
    sb ++= "  LOCAL VARS = " ++= hcfg.localVars.mkString(", ") ++= "\n"
    sb ++= "  ALLOCAS    = " ++= hcfg.allocas.mkString(", ") ++= "\n"
    for (hbb <- hcfg.bbs) {
      prettyPrint(hbb)
    }
    sb ++= "}\n"
    sb
  }

  def prettyPrint(hbb: HBB)(implicit sb: StringBuilder): StringBuilder = {
    sb ++= "  " ++= hbb.name ++= ":\n"
    sb ++= "        " ++= "idom: " ++= hbb.idom.map(_.name).getOrElse("None") ++= "\n"
    sb ++= "        " ++= "chld: " ++= hbb.children.map(_.name).mkString(", ") ++= "\n"
    for (hphi <- hbb.phis) {
      prettyPrint(hphi)
    }
    for (hinst <- hbb.insts) {
      prettyPrint(hinst, last = hinst eq hbb.lastInst)
    }

    sb
  }
  
  def hTypeAndRepr(hv: HValue): String = {
    s" -- hTy: ${hv.hType.toString}; hRepr = ${hv.repr.toString()}" 
  }
  

  def prettyPrint(hphi: HPhi)(implicit sb: StringBuilder): StringBuilder = {
    sb ++= s"    PHI ${hphi.name} {" ++= hTypeAndRepr(hphi) ++= "\n"
    for ((s, v) <- hphi.bbMap) {
      sb ++= s"      ${s.name} -> ${v.name}\n"
    }
    sb ++= "    }\n"
    sb
  }

  def prettyPrint(hinst: HInst, last: Boolean = false)(implicit sb: StringBuilder): StringBuilder = {
    if (last) {
      sb ++= "   >"
    } else {
      sb ++= "    "
    }

    if (hinst == null) {
      sb ++= "NO LAST INSTRUCTION!!!\n"
      return sb
    }

    sb ++= s"${hinst.name.padTo(30, ' ')} = "

    sb ++= hinst.getClass.getSimpleName ++= "("
    sb ++= (hinst match {
      case i: HParam             => i.index.toString
      case i: HOSRParam          => i.index.toString

      case i: HConstLong         => i.value.toString
      case i: HConstBoolean      => i.value.toString
      case i: HConstDouble       => i.value.toString
      case i: HConstString       => i.value
      case i: HConstUndef        => ""
      case i: HConstNull         => ""
      case i: HConstClosure      => i.jsFunc.nameHint
      case i: HInstRet           => i.value.name
      case i: HInstBr            => i.bb.name
      case i: HInstBr2           => s"${i.cond.name}, ${i.ifTrue.name}, ${i.ifFalse.name}"
      case i: HInstBinOp         => s"${i.op.toString}, ${i.lhs.name}, ${i.rhs.name}"
      case i: HInstUnOp          => s"${i.op.toString}, ${i.opnd.name}"

      case i: HInstCall          => s"${i.callee.name}, [${i.args.map(_.name).mkString(", ")}]"

      case i: HInstLocalGet      => i.jsName
      case i: HInstLocalPut      => s"${i.jsName}, ${i.value.name}"
      case i: HInstGlobalGet     => i.jsName
      case i: HInstGlobalPut     => s"${i.jsName}, ${i.value.name}"
      case i: HInstPropGet       => s"${i.obj.name}, ${i.prop.name}"
      case i: HInstPropPut       => s"${i.obj.name}, ${i.prop.name}, ${i.value.name}"

      case i: HInstArrayNewEmpty => i.sz.name
      case i: HInstArrayNewLit   => s"[${i.elems.map(_.name).mkString(", ")}]"

      case i: HInstBail          => ""

      case i: HInstBox           => s"${i.opnd.name}"
    })

    sb ++= ")" ++= hTypeAndRepr(hinst) ++= "\n"
    sb
  }
}
