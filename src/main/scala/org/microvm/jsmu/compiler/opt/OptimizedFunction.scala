package org.microvm.jsmu.compiler.opt

import org.microvm.jsmu.client.JSFunction
import org.microvm.jsmu.compiler.JSTypes.JSTypes

import org.microvm.jsmu.muir.MuFunc

import jdk.nashorn.internal.ir.Node

class OptimizedFunction(val jsFunc: JSFunction, val maybeOsrInfo: Option[OSRInfo] = None) {
  var hCfg: HCFG = _
  var muFunc: MuFunc = _
}

class OSRInfo(val entryPoint: Node, val varInfo: Map[String, JSTypes])
