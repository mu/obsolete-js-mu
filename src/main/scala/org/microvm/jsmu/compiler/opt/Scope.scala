package org.microvm.jsmu.compiler.opt

abstract class RValue
abstract class LValue extends RValue

case class ComputedValue(value: HValue) extends RValue // Computed result.
case class JSLocalVar(jsName: String) extends LValue   // Local variable.
case class JSGlobalVar(jsname: String) extends LValue  // Global variable.
case class PropAccess(obj: RValue, prop: RValue) extends LValue // A property access expression (a[b]).
