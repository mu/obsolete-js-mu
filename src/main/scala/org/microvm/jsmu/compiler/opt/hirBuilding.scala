package org.microvm.jsmu.compiler.opt

import scala.collection.mutable.ArrayBuffer

class HIRBuilder(val cfgName: String, val params: Seq[String], val localVars: Seq[String]) {
  val hCfg = new HCFG(cfgName)
  hCfg.params ++= params
  hCfg.localVars ++= localVars

  // entry may be changed if there is an OSR entry which replaces the regular entry.
  val normalEntry = new HBB("entry")
  hCfg.bbs += normalEntry
  hCfg.entry = normalEntry
  var curBB = normalEntry

  val bbNamer = new Namer("hbb")
  val instNamer = new Namer("hinst")

  def add(inst: HInst) {
    curBB.insts += inst
  }

  def emit(hint: String, inst: HInst): HInst = {
    val instName = instNamer.next(hint)
    inst.name = instName
    add(inst)
    inst
  }

  def use(bb: HBB) {
    curBB = bb
  }

  def newBB(hint: String): HBB = {
    val bbMuName = bbNamer.next(hint)
    val bb = new HBB(bbMuName)
    hCfg.bbs += bb
    bb
  }

  implicit class StringInstNamingSupport(str: String) {
    def :=[T <: HValue](value: T): T = {
      value.name = instNamer.next(str)
      value
    }

  }

}

class Namer(val prefix: String) {
  var num = 0

  def next(): String = {
    val myNum = num
    num += 1
    return prefix + num.toString
  }

  def next(nameHint: String): String = {
    val myNum = num
    num += 1
    return prefix + num.toString + "_" + nameHint
  }
}
