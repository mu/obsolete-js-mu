package org.microvm.jsmu.compiler.opt

import scala.collection.JavaConversions._

import org.microvm.jsmu.client._
import org.microvm.jsmu.client.JSFunction
import org.microvm.jsmu.compiler._

import HBinOp.HBinOp
import HUnOp.HUnOp
import jdk.nashorn.internal.ir._
import jdk.nashorn.internal.ir.LiteralNode._
import jdk.nashorn.internal.parser.TokenType

class OptimizingHIRBuilder(val optimizingCompiler: OptimizingCompiler,
                           val optFunc: OptimizedFunction) extends {
  val jsFunc = optFunc.jsFunc
  val nameHint = jsFunc.nameHint
} with HIRBuilder(nameHint, jsFunc.params, jsFunc.localVars) {
  val allLocalVars = jsFunc.getAllLocalVars()

  val jsClient = optimizingCompiler.jsClient
  val funcNode = jsFunc.funcNode
  val source = jsFunc.source

  class Scope() {
    def apply(jsName: String): LValue = {
      if (allLocalVars contains jsName) {
        JSLocalVar(jsName)
      } else {
        JSGlobalVar(jsName)
      }
    }
  }

  build()

  def build() {
    implicit val theScope = new Scope()

    makeAllocas()

    makeParams()

    initLocalVars()

    visitBlock(funcNode.getBody)

    emitEmptyReturn()
  }

  def makeAllocas() {
    // In the beginning, assume all local variables (including params) need ALLOCAs.
    // They are eliminated by running optimisation.
    hCfg.allocas ++= allLocalVars
  }

  def makeParams()(implicit scope: Scope) {
    if (optFunc.maybeOsrInfo.isDefined && optFunc.maybeOsrInfo.get.entryPoint == funcNode) {
      populateOSRParams()
    } else {
      for ((param, i) <- params.zipWithIndex) {
        val hp = emit(s"param_${i}", new HParam(i))
        val hs = emit(s"putParam_${param}", new HInstLocalPut(param, hp))
      }
    }
  }

  def initLocalVars()(implicit scope: Scope) {
    val cu = emitUndef()
    val cuHV = prepareRValue(cu)
    for (lv <- localVars) {
      emit(s"initVar_${lv}", new HInstLocalPut(lv, cuHV))
    }
  }

  /**
   * Make the OSR entry basic block and link hCfg.entry to that entry. The caller must assume curBB will be changed
   * by this function.
   */
  def makeOSREntry(node: Node, bbHead: HBB): Unit = {
    val osrEntry = newBB("osrentry")

    hCfg.entry = osrEntry

    use(osrEntry)

    populateOSRParams()

    emit("toLoopHeader", new HInstBr(bbHead))
  }

  def populateOSRParams() {
    for ((jsName, i) <- allLocalVars.zipWithIndex) {
      val osrVar = emit("extractOSRVar_" + jsName, new HOSRParam(i))
      emit("putOSRVar_" + jsName, new HInstLocalPut(jsName, osrVar))
    }
  }

  def visitBlock(block: Block)(implicit scope: Scope) {
    for (stmt <- block.getStatements) {
      visitStatement(stmt)
    }
  }

  def visitStatement(stmt: Statement)(implicit scope: Scope): Unit = stmt match {
    case s: ExpressionStatement => visitExpressionStatement(s)
    case s: VarNode             => visitVarNode(s)
    case s: IfNode              => visitIfNode(s)
    case s: WhileNode           => visitWhileNode(s)
    case s: ForNode             => visitForNode(s)
    case s: ReturnNode          => visitReturnNode(s)
    case _                      => throw new NotImplementedError("Other statements %s: %s".format(stmt, stmt.getClass) inCtx stmt)
  }

  def visitExpressionStatement(stmt: ExpressionStatement)(implicit scope: Scope) {
    val expr = stmt.getExpression

    val mnRv = visitExpression(expr)
  }
  def visitVarNode(stmt: VarNode)(implicit scope: Scope) {
    val lhsName = stmt.getName.getName

    val maybeRhsExpr = Option(stmt.getAssignmentSource)
    maybeRhsExpr match {
      case None =>
      case Some(rhsExpr) => {
        val rhsRv = visitExpression(rhsExpr)

        val lhsLv = scope(lhsName)

        emitAssign(stmt, "varNode", lhsLv, rhsRv)
      }
    }
  }

  def visitIfNode(ifNode: IfNode)(implicit scope: Scope): Unit = {
    val tst = ifNode.getTest
    val pas = ifNode.getPass
    val maybeFal = Option(ifNode.getFail)

    val tstRValue = visitExpression(tst)

    val tstHV = prepareRValue(tstRValue)
    val bbPass = newBB("ifPass")
    val maybeBBFail = if (maybeFal.nonEmpty) Some(newBB("ifFail")) else None
    val bbEnd = newBB("ifEnd")

    val tstBr = maybeBBFail match {
      case Some(bbFail) => emit("tstBrWithElse", new HInstBr2(tstHV, bbPass, bbFail))
      case None         => emit("tstBrNoElse", new HInstBr2(tstHV, bbPass, bbEnd))
    }

    use(bbPass)
    visitBlock(pas)
    emit("endIfFromPass", new HInstBr(bbEnd))

    if (maybeBBFail.nonEmpty) {
      use(maybeBBFail.get)
      visitBlock(maybeFal.get)
      emit("endIfFromFail", new HInstBr(bbEnd))
    }

    use(bbEnd)
  }

  def visitWhileNode(whileNode: WhileNode)(implicit scope: Scope): Unit = {
    if (whileNode.isDoWhile()) {
      throw new UnimplementedFeatureError(("Do-while loop") inCtx whileNode)
    }

    val tst = whileNode.getTest.getExpression
    val body = whileNode.getBody
    val beforeBack = () => {}
    generalLoop(whileNode, "while", Some(tst), body, beforeBack)
  }

  def visitForNode(forNode: ForNode)(implicit scope: Scope): Unit = {
    if (forNode.isForIn()) {
      throw new UnimplementedFeatureError(("For-in loop") inCtx forNode)
    }

    val maybeInit = Option(forNode.getInit)
    val maybeTst = Option(forNode.getTest).map(_.getExpression)
    val body = forNode.getBody
    val maybeNext = Option(forNode.getModify).map(_.getExpression)
    val beforeBack = () => { maybeNext foreach { next => visitExpression(next) } }

    maybeInit.foreach(init => visitExpression(init))
    generalLoop(forNode, "for", maybeTst, body, beforeBack)
  }

  def generalLoop(node: Node, kind: String, maybeTst: Option[Expression], body: Block, beforeBack: () => Unit)(implicit scope: Scope): Unit = {
    val bbHead = newBB(s"${kind}Head")
    emit(s"${kind}EnterHead", new HInstBr(bbHead))

    use(bbHead)

    val bbBody = newBB(s"${kind}Body")
    val bbEnd = newBB(s"${kind}End")

    maybeTst match {
      case Some(tst) => {
        val tstRValue = visitExpression(tst)

        val tstHV = prepareRValue(tstRValue)
        emit(s"${kind}Branch", new HInstBr2(tstHV, bbBody, bbEnd))
      }
      case None => {
        emit(s"${kind}BranchUncond", new HInstBr(bbBody))
      }
    }

    use(bbBody)
    visitBlock(body)

    beforeBack()

    emit(s"${kind}BackToHead", new HInstBr(bbHead))

    // If osrInfo exists and the current loop is where it is triggered, then generate an OSR entry.
    // There is either no such OSR entry (when opt-compiling a function) or exactly one entry (optimisation triggered
    // by loop back-edge profiling).
    for (osrInfo <- optFunc.maybeOsrInfo if node == osrInfo.entryPoint) {
      makeOSREntry(node, bbHead)
    }

    use(bbEnd)
  }

  def visitReturnNode(returnNode: ReturnNode)(implicit scope: Scope): Unit = {
    if (returnNode.hasExpression()) {
      val expr = returnNode.getExpression
      val exprRValue = visitExpression(expr)
      val exprHV = prepareRValue(exprRValue)
      emit("return", new HInstRet(exprHV))
    } else {
      emitEmptyReturn()
    }
  }

  def visitExpression(expr: Expression)(implicit scope: Scope): RValue = expr match {
    case e: LiteralNode[_] => visitLiteral(e)
    case e: CallNode       => visitCall(e)
    case e: IdentNode      => visitIdent(e)
    case e: BinaryNode     => visitBinary(e)
    case e: UnaryNode      => visitUnary(e)
    case e: FunctionNode   => visitFunction(e)
    case e: IndexNode      => visitIndex(e)
    case _                 => throw new UnimplementedFeatureError("Other expression %s: %s".format(expr, expr.getClass) inCtx expr)
  }

  def visitLiteral(ln: LiteralNode[_])(implicit scope: Scope): RValue = {
    if (ln.isNull()) {
      emitNull()
    } else if (ln.isNumeric()) {
      emitDouble(ln.getNumber())
    } else if (ln.isString()) {
      emitString(ln.getString())
    } else if (ln.isInstanceOf[ArrayLiteralNode]) {
      val al = ln.asInstanceOf[ArrayLiteralNode]
      val values = al.getValue

      val valHVs = values map { e =>
        val rv = visitExpression(e)
        val hv = prepareRValue(rv)
        hv
      }

      emitArrayNewLit(valHVs)
    } else {
      val obj = ln.getObject()
      obj match {
        case b: java.lang.Boolean => {
          if (b == true) {
            emitTrue()
          } else {
            emitFalse()
          }
        }
        case _ => throw new UnimplementedFeatureError(("Other constants " + ln) inCtx ln)
      }
    }
  }

  def visitCall(cn: CallNode)(implicit scope: Scope): RValue = {
    val calleeExpr = cn.getFunction()

    calleeExpr match {
      case i: IdentNode => {
        if (i.getName == "Array") {
          return emitArrayNewEmpty(cn)
        }
      }
      case _ =>
    }

    val callee = visitExpression(calleeExpr)
    val args = cn.getArgs().map(arg => visitExpression(arg))

    val calleeHV = prepareRValue(callee)
    val argsHV = args.map(arg => prepareRValue(arg))

    val call = emit("call", new HInstCall(calleeHV, argsHV))
    ComputedValue(call)
  }

  def visitIdent(idn: IdentNode)(implicit scope: Scope): LValue = {
    scope(idn.getName)
  }

  def visitBinary(bn: BinaryNode)(implicit scope: Scope): RValue = {
    val tokTy = bn.tokenType()

    tokTy match {
      case TokenType.ASSIGN    => visitAssignment(bn)
      case TokenType.ADD       => visitBinaryEager(bn, "add", HBinOp.ADD, flip = false, negate = false)
      case TokenType.SUB       => visitBinaryEager(bn, "add", HBinOp.SUB, flip = false, negate = false)
      case TokenType.MUL       => visitBinaryEager(bn, "add", HBinOp.MUL, flip = false, negate = false)
      case TokenType.DIV       => visitBinaryEager(bn, "add", HBinOp.DIV, flip = false, negate = false)
      case TokenType.EQ        => visitBinaryEager(bn, "eqa", HBinOp.EQ, flip = false, negate = false)
      case TokenType.EQ_STRICT => visitBinaryEager(bn, "eqs", HBinOp.EQ, flip = false, negate = false)
      case TokenType.NE        => visitBinaryEager(bn, "nea", HBinOp.EQ, flip = false, negate = true)
      case TokenType.NE_STRICT => visitBinaryEager(bn, "nes", HBinOp.EQ, flip = false, negate = true)
      case TokenType.LT        => visitBinaryEager(bn, "lt", HBinOp.LT, flip = false, negate = false)
      case TokenType.GT        => visitBinaryEager(bn, "gt", HBinOp.LT, flip = true, negate = false)
      case TokenType.LE        => visitBinaryEager(bn, "le", HBinOp.LT, flip = true, negate = true)
      case TokenType.GE        => visitBinaryEager(bn, "ge", HBinOp.LT, flip = false, negate = true)
      case _                   => throw new UnimplementedFeatureError(("Other binary operator " + tokTy) inCtx bn)
    }
  }

  def visitAssignment(bn: BinaryNode)(implicit scope: Scope): RValue = {
    val lhs = bn.getAssignmentDest
    val lhsRValue = visitExpression(lhs)

    lhsRValue match {
      case lhsLValue: LValue => {

        val rhs = bn.getAssignmentSource
        val rhsRValue = visitExpression(rhs)

        emitAssign(bn, "assignment", lhsLValue, rhsRValue)

        rhsRValue
      }
      case _ => {
        throw new OptimizingCompilerError(s"Assign to non-lvalue." inCtx lhs)
      }
    }
  }

  def visitBinaryEager(bn: BinaryNode, nameHint: String, hBinOp: HBinOp, flip: Boolean, negate: Boolean)(implicit scope: Scope): RValue = {
    val (realLhs, realRhs) = if (flip) (bn.rhs(), bn.lhs()) else (bn.lhs(), bn.rhs())
    val lhsRV = visitExpression(realLhs)
    val lhsHV = prepareRValue(lhsRV)
    val rhsRV = visitExpression(realRhs)
    val rhsHV = prepareRValue(rhsRV)

    val resHV = emit(nameHint, new HInstBinOp(hBinOp, lhsHV, rhsHV))

    val realResHV = if (negate) {
      val r = emit(nameHint + "Neg", new HInstUnOp(HUnOp.NOT, resHV))
      r
    } else {
      resHV
    }

    ComputedValue(realResHV)
  }

  def visitUnary(un: UnaryNode)(implicit scope: Scope): RValue = {
    val tokTy = un.tokenType()

    tokTy match {
      case TokenType.NOT        => visitUnaryEager(un, "not", HUnOp.NOT)
      case TokenType.SUB        => visitUnaryEager(un, "neg", HUnOp.NEG)
      case TokenType.INCPREFIX  => visitUnaryIncDec(un, isInc = true, isPrefix = true)
      case TokenType.DECPREFIX  => visitUnaryIncDec(un, isInc = false, isPrefix = true)
      case TokenType.INCPOSTFIX => visitUnaryIncDec(un, isInc = true, isPrefix = false)
      case TokenType.DECPOSTFIX => visitUnaryIncDec(un, isInc = false, isPrefix = false)
      case _                    => throw new UnimplementedFeatureError(("Other unary operator " + tokTy) inCtx un)
    }
  }

  def visitUnaryEager(un: UnaryNode, nameHint: String, hUnOp: HUnOp)(implicit scope: Scope): RValue = {
    val opnd = un.getExpression()
    val opndRV = visitExpression(opnd)
    val opndHV = prepareRValue(opndRV)

    val resHV = emit(nameHint, new HInstUnOp(hUnOp, opndHV))

    ComputedValue(resHV)
  }

  def visitUnaryIncDec(un: UnaryNode, isInc: Boolean, isPrefix: Boolean)(implicit scope: Scope): RValue = {
    val opnd = un.getExpression()
    val opndRV = visitExpression(opnd)
    opndRV match {
      case opndLV: LValue => {
        val opndHV = prepareRValue(opndLV)
        val rhsRV = if (isInc) emitDouble(1.0) else emitDouble(-1.0)
        val rhsHV = prepareRValue(rhsRV)
        val resultMuName = emit("incDec", new HInstBinOp(HBinOp.ADD, opndHV, rhsHV))

        emitAssign(un, "incDecStore", opndLV, ComputedValue(resultMuName))

        ComputedValue(if (isPrefix) resultMuName else opndHV)
      }
      case _ => {
        throw new OptimizingCompilerError(s"Inc/dec with non-lvalue." inCtx un)
      }
    }
  }

  def visitFunction(fn: FunctionNode)(implicit scope: Scope): RValue = {
    val nameHint = fn.getName

    val jsFunc = jsClient.funcManager.jsFunctions(fn)

    val closureRValue = emitClosure(jsFunc)
    closureRValue
  }

  def visitIndex(ino: IndexNode)(implicit scope: Scope): LValue = {
    val base = ino.getBase
    val index = ino.getIndex

    val baseRValue = visitExpression(base)
    val indexRValue = visitExpression(index)

    PropAccess(baseRValue, indexRValue)
  }

  def emitUndef(): RValue = {
    val res = emit("emitUndef", new HConstUndef())
    ComputedValue(res)
  }

  def emitNull(): RValue = {
    val res = emit("emitNull", new HConstNull())
    ComputedValue(res)
  }

  def emitFalse(): RValue = {
    val res = emit("emitFalse", new HConstBoolean(false))
    ComputedValue(res)
  }

  def emitTrue(): RValue = {
    val res = emit("emitTrue", new HConstBoolean(true))
    ComputedValue(res)
  }

  def emitDouble(num: Double): RValue = {
    val res = emit("emitDouble", new HConstDouble(num))
    ComputedValue(res)
  }

  def emitString(str: String): RValue = {
    val res = emit("emitString", new HConstString(str))
    ComputedValue(res)
  }

  def emitClosure(jsFunc: JSFunction): RValue = {
    val res = emit("emitClosure", new HConstClosure(jsFunc))
    ComputedValue(res)
  }

  def emitEmptyReturn() {
    val undef = emit("retUndef", new HConstUndef())
    emit("emptyReturn", new HInstRet(undef))
  }

  def emitAssign(node: Node, nameHint: String, lhs: LValue, rhs: RValue): Unit = {
    lhs match {
      case JSLocalVar(jsName) => {
        val rhsHV = prepareRValue(rhs)
        emit(nameHint, new HInstLocalPut(jsName, rhsHV))
      }
      case JSGlobalVar(jsName) => {
        val rhsHV = prepareRValue(rhs)
        emit(nameHint, new HInstGlobalPut(jsName, rhsHV))
      }
      case PropAccess(obj, prop) => {
        val objHV = prepareRValue(obj)
        val proHV = prepareRValue(prop)
        val valHV = prepareRValue(rhs)
        emit(nameHint, new HInstPropPut(objHV, proHV, valHV))
      }
    }
  }

  /**
   * The Array(n) call.
   */
  def emitArrayNewEmpty(cn: CallNode)(implicit scope: Scope): RValue = {
    val args = cn.getArgs()
    if (args.size != 1) {
      throw new UnimplementedFeatureError("Currently the Array() call only accepts the length argument." inCtx cn)
    }

    val arg = args(0)
    val argRValue = visitExpression(arg)
    val argHValue = prepareRValue(argRValue)

    val res = emit("arrayNew", new HInstArrayNewEmpty(argHValue))
    ComputedValue(res)
  }

  /**
   * The Array(n) call.
   */
  def emitArrayNewLit(elems: Seq[HValue])(implicit scope: Scope): RValue = {
    val res = emit("arrayLit", new HInstArrayNewLit(elems))
    ComputedValue(res)
  }
  /**
   * Make the RValue available as a HValue.
   * <p>
   * Specifically, if the RValue is an LValue, it needs to be loaded.
   *
   * @return a HValue.
   */
  def prepareRValue(rv: RValue): HValue = rv match {
    case ComputedValue(v) => v
    case JSLocalVar(jsName) => {
      val i = emit("loadLocalVar_" + jsName, new HInstLocalGet(jsName))
      i
    }
    case JSGlobalVar(jsName) => {
      val i = emit("loadGlobalVar_" + jsName, new HInstGlobalGet(jsName))
      i
    }
    case PropAccess(baseRV, indexRV) => {
      val baseHV = prepareRValue(baseRV)
      val indexHV = prepareRValue(indexRV)
      val i = emit("propGet", new HInstPropGet(baseHV, indexHV))
      i
    }
  }

  implicit class StringInContextSupport(msg: String) {
    def inCtx(n: Node): String = {
      val sPos = n.getStart()
      val sLine = source.getLine(sPos);
      val sCol = source.getColumn(sPos);

      val marker = SourceHelper.marker(source, n)
      "%d:%d: %s\n%s".format(sLine, sCol, msg, marker)
    }
  }
}
