package org.microvm.jsmu.compiler.opt

import org.microvm.jsmu.compiler.JSTypes
import org.microvm.jsmu.compiler.JSTypes.JSTypes

/**
 * Indicates what data type a HValue is holding or produces.
 * <p>
 * The following cases form a lattice with HNoInfo on the top and HPolymorphic at the bottom.
 * Everything in the middle are conceptually "on the same level".
 */
abstract class HValueType(val level: Int) {
  def isAbove(that: HValueType): Boolean = level < that.level
}

/** No information about the type yet. */
case object HNoInfo extends HValueType(0)
/** Not producing any value. */
case object HVoid extends HValueType(1)
/** A 64-bit integer. Not a JS value. */
case object HInt64 extends HValueType(1)
/** May hold exact one JS type. */
case class HJSType(val jsType: JSTypes) extends HValueType(1)
/** May hold more than one JS types. */
case object HPolymorphic extends HValueType(2)

/** How the value produced by a HValue should be represented as Mu types. */
object HValueRepr extends Enumeration {
  type HValueRepr = Value
  /** The corresponding Mu instruction has void type. */
  val NO_REPR = Value
  /**
   * This HValue is omitted. i.e. prevented from generating Mu instructions.
   *  It is usually because it is merged with another HInst (e.g. LT + NOT = GE).
   */
  val OMITTED = Value
  val AS_TAGREF, AS_DOUBLE, AS_I1, AS_I64, AS_REFSTRING, AS_REFCLOSURE = Value
}

