package org.microvm.jsmu.compiler.opt

import scala.collection.JavaConversions._

import org.microvm.jsmu.client._
import org.microvm.jsmu.compiler._
import org.microvm.jsmu.muir.MuNamer
import org.slf4j.LoggerFactory

import com.typesafe.scalalogging.Logger

import jdk.nashorn.internal.ir._
import jdk.nashorn.internal.ir.LiteralNode._

object OptimizingCompiler {
  val logger: Logger = Logger(LoggerFactory.getLogger(getClass.getName))
}

class OptimizingCompiler(val jsClient: JSClient) {
  import OptimizingCompiler._
  val optFuncNamer = new MuNamer("@optfunc")

  def compileFunction(jsFunc: JSFunction, maybeOsrInfo: Option[OSRInfo] = None, showHIR: Boolean = false,
       showMuIR: Boolean = false, optimize: Boolean = true, eager: Boolean = false): OptimizedFunction = {
    logger.debug("Optimizing compiling function " + jsFunc.nameHint)
    logger.debug {
      maybeOsrInfo match {
        case None => "OSR info not available."
        case Some(osrInfo) => {
          val sb = new StringBuilder("OSR info available.\n")
          val entryKind = if(osrInfo.entryPoint==jsFunc.funcNode) "func entry" else "loop header"
          sb ++= "Entry: " ++= entryKind ++= "\n"
          sb ++= SourceHelper.richerMarker(jsFunc.source, osrInfo.entryPoint) ++= "\n"
          sb ++= "Local variables:\n"
          for ((jsName, jsType) <- osrInfo.varInfo) {
            sb ++= "%s: %s\n".format(jsName, jsType.toString())
          }
          sb.toString()
        }
      }
    }

    val optFunc = new OptimizedFunction(jsFunc, maybeOsrInfo)
    jsFunc.optFunc = Some(optFunc)

    makeHIR(optFunc)

    if (showHIR) {
      println("Initial HIR graph")
      val cfgStr = HIR.prettyPrint(optFunc.hCfg)(new StringBuilder).toString()
      println(cfgStr)
    }

    if (optimize) {
      optimizeHIR(optFunc, showHIR = showHIR)
    }

    // Currently depends on the fact that BaselineCompiler puts callees first in the bu.funcs list
    // to ensure the callee is opt-compiled before the caller. So eager = true.
    genMuCode(optFunc, eager)
    
    val muIR = optFunc.muFunc.toMuIR()
    
    if (showMuIR) {
      println("Optimising compiler generated Mu IR")
      println(muIR)
    }
    
    // Load the optimized function
    jsClient.ca.loadBundle(muIR)

    optFunc
  }

  def makeHIR(optFunc: OptimizedFunction): Unit = {
    val hirBuilder = new OptimizingHIRBuilder(this, optFunc)
    val hCfg = hirBuilder.hCfg
    optFunc.hCfg = hCfg
  }

  def optimizeHIR(optFunc: OptimizedFunction, showHIR: Boolean = false): Unit = {
    val ps: Seq[OptimizingPass] = Seq(BasicBlockTrim, RemoveUnreachableBasicBlocks, ToSSA,
        InferHValueTypes, AddBoxing)
    for (p <- ps) {
      p(optFunc)
      if (showHIR) {
        println("After " + p.getClass.getSimpleName)
        val cfgStr = HIR.prettyPrint(optFunc.hCfg)(new StringBuilder).toString()
        println(cfgStr)
      }
    }
  }

  def genMuCode(optFunc: OptimizedFunction, eager: Boolean = false): Unit = {
    val hirToMuIR = new HIRToMuIR(this, optFunc, eager)
    val muFunc = hirToMuIR.func
    optFunc.muFunc = muFunc
  }
}

class OptimizingCompilerError(msg: String = null, cause: Throwable = null) extends RuntimeException(msg, cause)
class UnimplementedFeatureError(msg: String = null, cause: Throwable = null) extends OptimizingCompilerError(msg, cause)
