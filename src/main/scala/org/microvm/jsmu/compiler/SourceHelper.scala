package org.microvm.jsmu.compiler

import jdk.nashorn.internal.ir.Node
import jdk.nashorn.internal.runtime.Source

object SourceHelper {
  def marker(source: Source, node: Node): String = {
    val sPos = node.getStart()
    val fPos = node.getFinish()
    val sLine = source.getLine(sPos);
    val sCol = source.getColumn(sPos);
    val fLine = source.getLine(fPos);
    val fCol = source.getColumn(fPos);
    val srcLine = source.getSourceLine(sPos)
    val lineLen = srcLine.length
    val markEnd = if (sLine == fLine) fCol else lineLen
    val marker = " " * sCol + "^" + "~" * (markEnd - sCol - 1)

    "%s\n%s".format(srcLine, marker)
  }
  
  def richerMarker(source: Source, node: Node): String = {
    val sPos = node.getStart()
    val sLine = source.getLine(sPos);
    val sCol = source.getColumn(sPos);
    val mkr = marker(source, node)
    
    "In source code %d:%d\n%s".format(sLine, sCol, mkr)
  }
}
