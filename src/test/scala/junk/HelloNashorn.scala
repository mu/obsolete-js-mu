package junk

import jdk.nashorn.internal.parser.Parser
import jdk.nashorn.internal.ir._
import jdk.nashorn.internal.runtime.ScriptEnvironment
import java.io.PrintWriter
import jdk.nashorn.internal.runtime.options.Options
import java.io.Reader
import jdk.nashorn.internal.runtime.Source
import jdk.nashorn.internal.ir.debug.PrintVisitor
import jdk.nashorn.internal.runtime.ErrorManager
import java.io.StringReader
import scala.collection.JavaConversions._

object HelloNashorn extends App {

  val code = """
    
    var a=1, b="foo", c=true;
    
    var d=null, e=false, f=true;
    
    var hw = "Hello world";
    
    function fac(n) {
      var result = 1;
      for (var i = 2; i <= n; i++) {
        result *= i;
      }
      return result;
    }
    
    function fac_rec(n) {
      if (n == 0) {
        return 1;
      } else {
        return n * fac_rec(n-1);
      }
    }
    
    print(hw, fac(4), fac_rec(5));
    
    var offset = 3;
    function addTo(n) {
      return n + offset;
    }
    
    offset = 4;
    
    print(addTo(offset));
    
    function foo() {}
    bar = function() {}
    var baz = function() {}
    
    var ar = [1,2,3];
    ar[1] = function(a,r,j) {}
    
    var ar1 = ar[1];
    print(ar1);
    """

  def parse(c: String): FunctionNode = {
    val se = new ScriptEnvironment(
      new Options("nashorn"), new PrintWriter(System.out), new PrintWriter(System.err))

    val sr = new StringReader(c)
    val src = Source.sourceFor("my-input", sr)
    val em = new ErrorManager()

    val p = new Parser(se, src, em)
    p.parse()
  }
  
  val fn = parse(code)

  val pv = new PrintVisitor(fn)
  println(pv.toString())
  
  for (stmt <- fn.getBody.getStatements) {
    stmt match {
      case s: VarNode => {
        val lName = s.getAssignmentDest.getName
        val rVal = s.getAssignmentSource
        val rType = rVal.getClass
        println("VarNode: %s = %s : %s".format(lName, rVal, rType))
      }
      case _ => println(stmt)
    }
  }
}
