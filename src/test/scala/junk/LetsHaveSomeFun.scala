package junk

object LetsHaveSomeFun extends App {
  LawBreakingRunner runFile "uir/tests/factorial_loop.js"
}

object LetsHaveSomeRecursiveFun extends App {
  LawBreakingRunner runFile "uir/tests/factorial_rec.js"
}
