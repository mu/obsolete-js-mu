package junk

import uvm.refimpl.MicroVM
import org.microvm.jsmu.client.JSClient

object HelloOptCompiler extends App with org.microvm.jsmu.LogSetter {
  import ch.qos.logback.classic.Level._
  setLogLevels(
    ROOT_LOGGER_NAME -> INFO,
    "uvm.refimpl" -> INFO,
    "uvm.refimpl.mem" -> INFO,
    "uvm.refimpl.itpr" -> INFO,
    "org.microvm.jsmu" -> DEBUG)

  val microVM = new MicroVM()

  val jsClient = new JSClient(microVM)

  val script = scala.io.Source.fromFile("uir/tests/hellooptcomp.js").mkString

  jsClient.executeWithOpt(script)
}
