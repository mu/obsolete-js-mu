package junk

import uvm.refimpl.MicroVM
import org.microvm.jsmu.client.JSClient

object HelloWorldWithMuJs extends App {
  val microVM = new MicroVM()
  
  val jsClient = new JSClient(microVM)
  
  val hwScript = scala.io.Source.fromFile("uir/tests/helloworld.js").mkString
  
  jsClient.execute(hwScript)
}
