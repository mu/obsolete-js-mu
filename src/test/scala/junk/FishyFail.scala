package junk

import uvm.refimpl.MicroVM
import org.microvm.jsmu.client.JSClient

/**
 * This showed a bug in Mu refimpl itself where SSAVariables are not properly compared for equality.
 */
object FishyFail extends App {
  val microVM = new MicroVM()

  val jsClient = new JSClient(microVM)

  val script = """
//v0 = "blah0";
//v1 = "blah1";
//v2 = "blah2";
//v3 = "blah3";
//v4 = "blah4";
//v5 = "blah5";
//v6 = "blah6";
v0 = 0;
v1 = 1;
v2 = 2;
v3 = 3;
v4 = 4;
v5 = 5;
v6 = 6;
v7 = 7;
v8 = 8;
v9 = 9;
v10 = 10;
v11 = 11;
v12 = 12;
v13 = 13;
v14 = 14;
v15 = 15;
v16 = 16;
v17 = 17;
v18 = 18;



    """

  jsClient.execute(script)
}
