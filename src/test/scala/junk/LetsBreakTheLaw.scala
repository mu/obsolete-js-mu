package junk

import uvm.refimpl.MicroVM
import org.microvm.jsmu.client.JSClient

object LawBreakingRunner {
  def run(script: String) {
    val microVM = new MicroVM()
    val jsClient = new JSClient(microVM)
    jsClient.execute(script)
  }
  def runFile(fileName: String) {
    run(scala.io.Source.fromFile(fileName).mkString)
  }
}

object LetsBreakTheLaw extends App {
  LawBreakingRunner runFile "uir/tests/illegalcall.js"
}

object LetsBreakTheLaw2 extends App {
  LawBreakingRunner run """ "foo" ("bar"); """
}

object LetsBreakTheGrammar extends App {
  LawBreakingRunner run """41231234 = 9;"""
}

object LetsBreakBinaryOp extends App {
  LawBreakingRunner run """var x = 3 + "foo";"""
}

object LetsBreakBinaryOpTake2 extends App {
  LawBreakingRunner run """var x = "bar" + true;"""
}

object LetsBreakBinaryOpTake3 extends App {
  LawBreakingRunner run """var x = "bar" + 42;"""
}

object LetsBreakBinaryOpTake4 extends App {
  LawBreakingRunner run """var x = null + undefined;"""
}

object LetsBreakCmpClosure extends App {
  LawBreakingRunner run """var x = print == 42;"""
}

object LetsBreakLt extends App {
  LawBreakingRunner run """print(4 < "hello");""" // real js should not break
  LawBreakingRunner run """var x = true > "hello";""" // real js should not break
}

object LetsBreakNot extends App {
  LawBreakingRunner run """print(!"hello");""" // real js should not break
  LawBreakingRunner run """var x = !(3+5);""" // real js should not break
}

object LetsBreakNeg extends App {
  LawBreakingRunner run """print(-"hello");"""
  LawBreakingRunner run """var x = -true;"""
}

object LetsBreakIf extends App {
  LawBreakingRunner run """if(14 + 28) print("you think 42 is a boolean?");"""
}

object LetsBreakWhile extends App {
  LawBreakingRunner run """while(42) print("you think 42 is a boolean?");"""
}

object LetsBreakFor extends App {
  LawBreakingRunner run """for(;42;) print("you think 42 is a boolean?");"""
}

object LetsLoveForEver extends App {
  LawBreakingRunner run """for(i=0;;i=i+1) print("Hello", i);"""
}

object LetsStayForEver extends App {
  LawBreakingRunner run """while(42); for(i=0;;i=i+1) ;"""
}

object LetsDoSomeFancyFor extends App {
  LawBreakingRunner run """for(i in 42) {}"""
}

object LetsDoSomeFancyFor2 extends App {
  LawBreakingRunner run """for(j of 43) {}"""
}

object LetsBreakIncDec extends App {
  LawBreakingRunner run """ 'c'++; """
}

object LetsBreakIncDec2 extends App {
  LawBreakingRunner run """ var c='c'; c++; """
}

object LetsBreakSub extends App {
  LawBreakingRunner run """ var c="foo"-1; // Sorry. No batman. """
}

object LetsBreakMul extends App {
  LawBreakingRunner run """ var c=1*"foo"; """
}

object LetsBreakDiv extends App {
  LawBreakingRunner run """ var c=4/ false; """
}
