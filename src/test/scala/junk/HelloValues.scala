package junk

import uvm.refimpl.MicroVM
import org.microvm.jsmu.client.JSClient

object HelloValues extends App {
  val microVM = new MicroVM()
  
  val jsClient = new JSClient(microVM)
  
  val script = scala.io.Source.fromFile("uir/tests/hellovalues.js").mkString
  
  jsClient.execute(script)
}
