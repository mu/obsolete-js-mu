package org.microvm.jsmu

import org.scalatest._
import uvm.refimpl._
import org.microvm.jsmu.client._
import ch.qos.logback.classic.Level._

class JSMuTests extends FlatSpec with Matchers with LogSetter {
  setLogLevels(
    ROOT_LOGGER_NAME -> INFO,
    "uvm.refimpl" -> INFO,
    "uvm.refimpl.mem" -> INFO,
    "uvm.refimpl.itpr" -> INFO,
    "org.microvm.jsmu" -> DEBUG)

  def runScript(fileName: String) {
    uvm.refimpl.mem.MemorySupport.bb.clear()

    val microVM = new MicroVM()

    val jsClient = new JSClient(microVM)

    val script = scala.io.Source.fromFile(fileName).mkString

    jsClient.execute(script, allowOsr = false)
  }

  behavior of "JSClient"

  it should "run hello world" in runScript("uir/tests/helloworld.js")
  it should "print literals" in runScript("uir/tests/hellovalues.js")
  it should "support local variables" in runScript("uir/tests/hellovariables.js")
  it should "support global variables" in runScript("uir/tests/helloglobalvars.js")
  it should "support binary operators" in runScript("uir/tests/hellobinops.js")
  it should "support if statement" in runScript("uir/tests/hellocondition.js")
  it should "support loops statement" in runScript("uir/tests/hellomanyworlds.js")
  it should "support functions" in runScript("uir/tests/hellofuncs.js")
  it should "support arrays" in runScript("uir/tests/helloarrays.js")
}
