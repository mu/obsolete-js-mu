package org.microvm.jsmu

import org.scalatest._
import uvm.refimpl._
import org.microvm.jsmu.client._
import ch.qos.logback.classic.Level._

class JSMuOptTests extends FlatSpec with Matchers with LogSetter {
  setLogLevels(
    ROOT_LOGGER_NAME -> INFO,
    "uvm.refimpl" -> INFO,
    "uvm.refimpl.mem" -> INFO,
    "uvm.refimpl.itpr" -> INFO,
    "org.microvm.jsmu" -> DEBUG)

  def runScript(fileName: String) {
    uvm.refimpl.mem.MemorySupport.bb.clear()

    val microVM = new MicroVM()

    val jsClient = new JSClient(microVM)

    val script = scala.io.Source.fromFile(fileName).mkString

    jsClient.executeWithOpt(script, showMuCode = true, execute = true)
  }

  def runScriptBaselineForceOSR(fileName: String) {
    uvm.refimpl.mem.MemorySupport.bb.clear()

    val microVM = new MicroVM()

    val jsClient = new JSClient(microVM)

    val script = scala.io.Source.fromFile(fileName).mkString

    jsClient.execute(script, execute = true, allowOsr = false, forceOSROnCall = true)
  }

  behavior of "optimizing passes"

  it should "eliminate instructions after the HLastInst" in runScript("uir/tests/opt/redundantret.js")
  it should "convert cfg to SSA" in runScript("uir/tests/opt/tossa.js")
  it should "infer types (swaps)" in runScriptBaselineForceOSR("uir/tests/opt/swaps.js")
  it should "infer types (multiply)" in runScriptBaselineForceOSR("uir/tests/opt/multiply.js")
}
