package org.microvm.jsmu

import ch.qos.logback.classic.Level

trait LogSetter {
  val ROOT_LOGGER_NAME = org.slf4j.Logger.ROOT_LOGGER_NAME

  def setLogLevels(settings: (String, Level)*): Unit = { // Configure logger
    import org.slf4j.LoggerFactory
    import org.slf4j.{ Logger => SLogger }
    import ch.qos.logback.classic.{ Logger => LLogger, Level }
    import ch.qos.logback.classic.Level._

    def setLevel(name: String, level: Level): Unit = {
      LoggerFactory.getLogger(name).asInstanceOf[LLogger].setLevel(level)
    }

    for ((name, lvl) <- settings) {
      setLevel(name, lvl)
    }
  }

}
