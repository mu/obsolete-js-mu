package org.microvm.jsmu

import org.scalatest._
import uvm.refimpl._
import org.microvm.jsmu.client._
import ch.qos.logback.classic.Level._

class JSMuOsrTests extends FlatSpec with Matchers with LogSetter {
  setLogLevels(
    ROOT_LOGGER_NAME -> INFO,
    "uvm.refimpl" -> INFO,
    "uvm.refimpl.mem" -> INFO,
    "uvm.refimpl.itpr" -> INFO,
    "org.microvm.jsmu" -> DEBUG)

  def runScript(fileName: String, allowOsr: Boolean) {
    uvm.refimpl.mem.MemorySupport.bb.clear()

    val microVM = new MicroVM()

    val jsClient = new JSClient(microVM)

    val script = scala.io.Source.fromFile(fileName).mkString

    jsClient.execute(script, execute = true, allowOsr = allowOsr)
  }

  behavior of "baseline-compiled code with OSR support"

  it should "identify hot loops and perform OSR" in runScript("uir/tests/osr/sum.js", allowOsr = true)
  it should "handle nested loop" in runScript("uir/tests/osr/nestedsum.js", allowOsr = true)

}
