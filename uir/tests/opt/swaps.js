// Swap x and y for c times and return x.
function swaps(x, y, c) {
    for (var i = 0; i < c; i++) {
        var tmp = x;
        x = y;
        y = tmp;
    }
    return x;
}

// The type inferer should deduce that both x and y are polymorphic.
var r1 = swaps(42, "foo", 3);

// The type inferer should deduce that both x and y are strings.
var r2 = swaps("bar", "oops", 3);

//The type inferer should deduce that both x and y are numbers.
var r3 = swaps(43, 99, 4);

print(r1, r2, r3);