// Multiply x by y.
// This function works when y is an integer while x can be either number or string.
// If x is a number, the result is x*y;
// if x is a string, the result is x repeated by y times.
function multiply(x, y) {
    var p = x;
    for (var i = 1; i < y; i++) {
        p = p + x;
    }
    return p;
}

var r1 = multiply(4, 3); // should infer x, p, y, i :: num

var r2 = multiply("foo", 3); // should infer x, p :: str; y, i :: num

print(r1, r2);