a = Array(15)
print(a[4])

a[4] = 42
print(a[4])

for (i = 0; i < 15; i++) {
	a[i] = i;
}

sum = 0;

for (i = 0; i < 15; i++) {
	sum = sum + i;
}

print(sum);

b = [ null, false, true, 3.14, "hello" ]
for (j = 0; j < 5; j++) {
	print(b[j]);
}