function sum(f, t) {
    var s = 0;
    for (var i = f; i <= t; i++) {
        for (var j = f; j <= t; j++) {
            s = s + i * j;
        }
    }
    return s;
}

var result = sum(1, 10);
print(result);