print("Hello, optimizing compiler!");

for (var i = 0; i < 10; i++) {
	if (i < 5) {
		print("Small", i);
	} else {
		print("Big", i);
	}
}

print("Good bye!");

foo = function(x, y) {
	var s = 0;
	for (var i = x; i < y; i++) {
		s = s + i
	}

	return s;
}

var result = foo(10, 20);
print(result);

var nonexist;
print(nonexist, "(should be undefined)");