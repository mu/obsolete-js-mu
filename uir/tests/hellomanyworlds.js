var i = 0;
while (i < 10) {
	print("Hello world!", i);
	i = i + 1;
}

var j;
for (j = 20; j > 0; j--) {
	print("Goodbye world...", j);
}

for (k = 0; k < 5;) {
	print("k = ", k);
	k = k + 2;
}

var l = 0, m = 0;
while(l < 5) {
	print("l = ", l++, ", m = ", ++m);
}
