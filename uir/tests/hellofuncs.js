foo = function () {
	print("Hello");
}

function bar(a,b) {
	return a+b;
}

foo();

var b = bar(3,4);
print(b);

var baz = function(n) {
	return n+1;
}

print(baz(9));
print(baz(99,false));

sumTo = function (n) {
	if (n == 0) {
		return 0;
	} else {
		return n + sumTo(n-1);
	}
}

print(sumTo(3));

printIt = function (x) {
	print(x)
}

printIt("HelloWorld");
printIt();