var a = 3;
var b = a + 4;
print("a + b = ", b);

var h = "Hello";
var w = "world";
print("h + w = ", h + w);

print("" + "foo")
print("bar" + "")
print("" + "")

var t = "(true)", f = "(false)";

print("equal");
print(1, 42 === 42, t);
print(2, 43 === 42, f);
// print(3, NaN === NaN, f); // ECMAScript does not define NaN literal.
print(4, "hello" === "hello", t);
print(5, "hello" === "world", f);
print(6, "hello" === "ltns", f);
print(7, "ltns" === "hello", f);

print(8, none == false, f);
print(9, false == false, t);

print(10, false != false, f);

print("less than");
print(1, 42 < 42, f);
print(2, 42 < 41, f);
print(3, 42 < 43, t);

print(4, 42 > 42, f);
print(5, 42 > 41, t);
print(6, 42 > 43, f);

print(7, 42 <= 42, t);
print(8, 42 <= 41, f);
print(9, 42 <= 43, t);

print(10, 42 >= 42, t);
print(11, 42 >= 41, t);
print(12, 42 >= 43, f);

print(13, "hell" < "hello", t);
print(14, "hello" < "hello", f);
print(15, "helloworld" < "hello", f);
print(16, "helen" < "hello", t);
print(17, "helen" > "hello", f);

print("unary not");
print(1, !true, f);
print(2, !false, t);

print("unary negation");
print(1, -1);
print(2, -3.14159);

print("other binary operations");
print("4 + 3 =", 4 + 3);
print("4 - 3 =", 4 - 3);
print("4 * 3 =", 4 * 3);
print("40 / 7 =", 40 / 7);
