var a = 42;
var z = "No!";
if (a == 42) {
	z = "Yes!";
}
print(z);

var b = 43;
if (b == 42) {
	z = "Still yes!";
} else {
	z = "No! No no no no no no no!";
}
print(z);

var c = 44;
var y;
if (c > 40) {
	if (c > 50) {
		z = "too big";
	} else {
		z = "not big enough";
	}

	y = "shouldReachHere";
}

print(y, z);

if (false) {
	print("this should do nothing");
}
print("QED.");