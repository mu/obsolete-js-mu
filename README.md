# js-mu: A small subset of JavaScript on Mu

This project is to demonstrate Mu can work as a substrate of dynamic language
implementations, and Mu's client interface can simplify the jobs of the client
so it can focus on the high-level aspects of language implementations rather
than messing with low-level stuff and machine-specific assembly.

This project depends on the Nashorn implementation in `OpenJDK 1.8.0_45`.

# Limitations

The main purpose of this project is to demonstrate stack introspection and
on-stack replacement. For simplicity, this project only implements a very small
set of ECMAScript.

* Arrays and objects are not implemented.
* All variables are either function-local or global, i.e. there is no closure.
  Shadowing a local variable is an error. Also undefined variables are global
  variables which all initially hold `undefined` values.
* Only a small set of operators are supported.
* This project does not try to optimise numbers to integer. The optimising
  compiler attempts to use raw `double` values rather than tagged references
  (`tagref64`).
* No exception handling, but there is a magic function that introspects the
  stack when an error occurs.

# License

Proprietary for now, but the Mu reference implementation v2 is licensed under CC
BY-SA.

# Author

Kunshan Wang <kunshan.wang@anu.edu.au>

<!--
vim: tw=80
-->
